# Разворачивание Chicago

Скачиваем скрипт автоматического разворачивания:

    wget https://bitbucket.org/korkholeh/sbits-host-panel/downloads/start-chicago.sh

(TODO): смотреть за развитием проекта, начиная с версии 0.3.1 сломан вывод логов.

Запускаем его на исполнение:

    chmod +x start-chicago.sh
    sudo ./start-chicago.sh

Скачиваем образы необходимые для функционирования платформы:

    sudo docker pull korkholeh/hipache:0.2
    sudo docker pull korkholeh/postgres93:0.2
    sudo docker pull korkholeh/webapp:0.1

Все описанные ниже пункты делает панель управления.

Устанавливаем Hipache:

    sudo docker run -d -p 80:80 -p 48999:22 --name="hipache" korkholeh/hipache:0.2 /sbin/my_init --

Запускаем сервис PostgreSQL:

    sudo docker run -d -p 49500:5432 -p 49501:22 -e PG_PASSWORD="qazwsx" --name="postgresql_01" korkholeh/postgres93:0.2 /sbin/my_init --

Здесь можно указать свой пароль для пользователя `postgres`. Название контейнера также должно быть уникально для всех контейнеров на всех серверах.

Старт контейнера с веб-приложением делается так:

    sudo docker run -d -p 49600:80 -p 49601:22 -p 49602:3131 --name="webapp_01" korkholeh/webapp:0.1 /sbin/my_init --
