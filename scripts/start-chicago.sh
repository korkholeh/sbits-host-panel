#!/bin/bash
echo "Start to build infrastructure type: Chicago"

DOCKER_HOST="127.0.0.1"

echo "Prepare the basic system"

sudo apt-get -qq update
sudo apt-get -y upgrade
sudo apt-get -y install mc htop openssh-server git postgresql-client-common postgresql-client python-software-properties build-essential redis-tools apache2-utils linux-headers-$(uname -r)
sudo apt-get -y install --no-install-recommends virtualbox-guest-utils && sudo apt-get -y install virtualbox-guest-dkms

echo "Install Docker"

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
sudo sh -c "echo deb http://get.docker.io/ubuntu docker main > /etc/apt/sources.list.d/docker.list"
sudo apt-get -qq update
sudo apt-get -y install lxc-docker cgroup-lite

echo "Install FTP"

sudo apt-get install vsftpd libpam-pwdfile
sudo mv /etc/vsftpd.conf /etc/vsftpd.conf.bak

sudo cat << EOT > /etc/vsftpd.conf
anonymous_enable=NO
local_enable=YES
chroot_local_user=YES
user_config_dir=/etc/vsftpd/vsftpd-virtual-user/
virtual_use_local_privs=YES
dual_log_enable=YES
connect_from_port_20=YES
listen=YES
pam_service_name=ftp
tcp_wrappers=YES
allow_writeable_chroot=YES
EOT

sudo restart vsftpd

sudo mkdir -p /etc/vsftpd/
sudo mkdir -p /etc/vsftpd/vsftpd-virtual-user/
sudo touch /etc/vsftpd/vsftpd-virtual-user/vsftpd_user

echo "Setup PAM"

sudo cp /etc/pam.d/vsftpd /etc/pam.d/vsftpd.bak
sudo cat << EOT > /etc/pam.d/vsftpd
session optional        pam_keyinit.so  force   revoke
auth   required        pam_listfile.so item=user sense=deny file=/etc/ftpusers onerr=succeed
auth   required        pam_shells.so
auth    include system-auth
account include system-auth
session include system-auth
session required pam_loginuid.so
EOT

echo "Prepare directories"

sudo mkdir -p /home/webprod/src
sudo mkdir -p /home/webprod/tmp
sudo mkdir -p /home/webprod/backups
sudo chown webprod:webprod /home/webprod/src
sudo chown webprod:webprod /home/webprod/tmp
sudo chown webprod:webprod /home/webprod/backups

echo "Setup complete. Good luck!"
