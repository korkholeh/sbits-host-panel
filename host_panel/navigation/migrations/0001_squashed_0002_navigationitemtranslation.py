# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    replaces = [('navigation', '0001_initial'), ('navigation', '0002_navigationitemtranslation')]

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='NavigationBar',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=u'Name')),
            ],
            options={
                u'verbose_name': u'Navigation bar',
                u'verbose_name_plural': u'Navigation bar',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NavigationItem',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('bar', models.ForeignKey(verbose_name=u'Navigation bar', to_field=u'id', blank=True, to='navigation.NavigationBar', null=True)),
                ('url', models.CharField(max_length=200, verbose_name=u'URL')),
                ('regex', models.CharField(max_length=200, verbose_name=u'Regex')),
                ('visible_in_top_bar', models.BooleanField(default=False, verbose_name=u'Visible in top bar')),
                ('position', models.IntegerField(default=1, verbose_name=u'Position')),
            ],
            options={
                u'ordering': ['position'],
                u'verbose_name': u'Navigation item',
                u'verbose_name_plural': u'Navigation items',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='NavigationItemTranslation',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=u'Name')),
                ('language_code', models.CharField(max_length=15, db_index=True)),
                ('master', models.ForeignKey(to_field=u'id', editable=False, to='navigation.NavigationItem', null=True)),
            ],
            options={
                u'unique_together': set([('language_code', 'master')]),
                u'db_table': u'navigation_navigationitem_translation',
            },
            bases=(models.Model,),
        ),
    ]
