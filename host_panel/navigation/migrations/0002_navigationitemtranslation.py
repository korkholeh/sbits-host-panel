# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('navigation', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='NavigationItemTranslation',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=u'Name')),
                ('language_code', models.CharField(max_length=15, db_index=True)),
                ('master', models.ForeignKey(to_field=u'id', editable=False, to='navigation.NavigationItem', null=True)),
            ],
            options={
                u'unique_together': set([('language_code', 'master')]),
                u'db_table': u'navigation_navigationitem_translation',
            },
            bases=(models.Model,),
        ),
    ]
