# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from hvad.models import TranslatableModel, TranslatedFields


class NavigationBar(models.Model):
    name = models.CharField(_(u'Name'), max_length=100)

    class Meta:
        verbose_name = _(u'Navigation bar')
        verbose_name_plural = _(u'Navigation bar')

    def __unicode__(self):
        return self.name


class NavigationItem(TranslatableModel):
    bar = models.ForeignKey(
        NavigationBar, verbose_name=_(u'Navigation bar'),
        related_name='items', null=True, blank=True)
    url = models.CharField(_(u'URL'), max_length=200)
    regex = models.CharField(_(u'Regex'), max_length=200)
    visible_in_top_bar = models.BooleanField(
        _(u'Visible in top bar'), default=False)
    position = models.IntegerField(_(u'Position'), default=1)

    translations = TranslatedFields(
        name=models.CharField(_(u'Name'), max_length=100),
    )

    class Meta:
        verbose_name = _(u'Navigation item')
        verbose_name_plural = _(u'Navigation items')
        ordering = ['position']

    def __unicode__(self):
        return self.name
