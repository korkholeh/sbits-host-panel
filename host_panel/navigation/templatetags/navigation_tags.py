# coding: utf-8
import re
from django import template
from navigation.models import NavigationItem


register = template.Library()


@register.inclusion_tag(
    'navigation/nav_side_bar.html', takes_context=True)
def nav_side_bar(context, bar=None, pk=None):
    request = context['request']
    if bar is None:
        items = NavigationItem.objects.all()
    else:
        items = NavigationItem.objects.filter(bar__name=bar)
    for item in items:
        pattern = re.compile(item.regex)
        m = re.match(pattern, request.get_full_path())
        if pk is not None:
            item.url_to_go = item.url.format(pk=pk)
        else:
            item.url_to_go = item.url
        if m:
            item.active = True
        else:
            item.active = False

    return {
        'items': items,
    }


@register.inclusion_tag(
    'navigation/nav_top_bar.html', takes_context=True)
def nav_top_bar(context):
    request = context['request']
    items = NavigationItem.objects.filter(visible_in_top_bar=True)
    for item in items:
        pattern = re.compile(item.regex)
        m = re.match(pattern, request.get_full_path())
        if m:
            item.active = True
        else:
            item.active = False

    return {
        'items': items,
    }
