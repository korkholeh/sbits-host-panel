# coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from hvad.admin import TranslatableAdmin
from .models import (
    NavigationBar,
    NavigationItem,
)


class NavigationBarAdmin(admin.ModelAdmin):
    list_display = ['name']

admin.site.register(NavigationBar, NavigationBarAdmin)


class NavigationItemAdmin(TranslatableAdmin):
    list_display = [
        'get_name', 'bar', 'url', 'regex',
        'visible_in_top_bar', 'position']
    search_fields = ['name']
    list_editable = ['bar', 'position']
    list_filter = ['bar']

    def get_name(self, obj):
        return obj.translations.get(language_code='en').name
    get_name.short_description = _(u'Name')

admin.site.register(NavigationItem, NavigationItemAdmin)
