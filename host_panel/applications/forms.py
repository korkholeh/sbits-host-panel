# coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit
from django.conf import settings
from crispy_forms.layout import (
    Layout,
    Field,
    MultiField,
)
from crispy_forms.bootstrap import AppendedText
from .models import (
    WebApplication,
    Domain,
    LinkedFTP,
    PostgreSQLUser,
    PostgreSQLDatabase,
    BackupSettings,
    ServerSettings,
    BackupToFTPLink,
)
from containers.models import DockerImage
# from django_select2.widgets import Select2MultipleWidget
from codemirror import CodeMirrorTextarea


class EasyConfigForm(forms.ModelForm):
    class Meta:
        model = ServerSettings
        fields = [
            'public_root', 'passenger_app_root', 'extra_top',
            'passenger_enabled', 'passenger_user',
            'passenger_friendly_error_pages', 'passenger_python',
            'passenger_max_pool_size',
            'passenger_min_instances',
            'passenger_pool_idle_time',
            'passenger_max_preloader_idle_time',
            'passenger_start_timeout',
            'passenger_max_requests',
            'passenger_pre_start',
            'passenger_max_request_queue_size',
            'passenger_request_queue_overflow_status_code',
            'passenger_ignore_client_abort',
            'passenger_intercept_errors',
            'passenger_pass_header',
            'passenger_buffer_response',
            'passenger_log_level', 'passenger_debug_log_file',
            'set_real_ip_from', 'real_ip_header', 'extra_tail',
            'auth_enabled',
            'auth_user',
            'auth_password',
            'auth_exlude_ips',
        ]
        widgets = {
            'extra_top': CodeMirrorTextarea(
                mode="nginx", config={'fixedGutter': True}),
            'extra_tail': CodeMirrorTextarea(
                mode="nginx", config={'fixedGutter': True}),
            # 'auth_password': forms.PasswordInput(),
        }

    def __init__(self, *args, **kwargs):
        super(EasyConfigForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-3'
        self.helper.field_class = 'col-lg-9'
        self.helper.layout = Layout(
            Fieldset(
                _(u'Project root'),
                'public_root',
                'passenger_app_root',
            ),
            Fieldset(
                _(u'Extra top'),
                'extra_top',
            ),
            Fieldset(
                _(u'Basic Passenger settings'),
                'passenger_enabled',
                'passenger_user',
                'passenger_friendly_error_pages',
                'passenger_python',
            ),
            Fieldset(
                _(u'Advanced Passenger settings'),
                'passenger_max_pool_size',
                'passenger_min_instances',
                'passenger_pool_idle_time',
                'passenger_max_preloader_idle_time',
                'passenger_start_timeout',
                'passenger_max_requests',
                'passenger_pre_start',
                'passenger_max_request_queue_size',
                'passenger_request_queue_overflow_status_code',
                'passenger_ignore_client_abort',
                'passenger_intercept_errors',
                'passenger_pass_header',
                'passenger_buffer_response',
            ),
            Fieldset(
                _(u'Debug log'),
                'passenger_log_level',
                'passenger_debug_log_file',
            ),
            Fieldset(
                _(u'IP fix'),
                'set_real_ip_from',
                'real_ip_header',
            ),
            Fieldset(
                _(u'Extra tail'),
                'extra_tail',
            ),
            Fieldset(
                _(u'Authentication'),
                'auth_enabled',
                'auth_user',
                'auth_password',
                'auth_exlude_ips',
            ),
            ButtonHolder(
                Submit('submit', _(u'Save'), css_class='btn btn-primary')
            )
        )


class EditServerConfigFileForm(forms.Form):
    config = forms.CharField(
        label=_(u'webapp.conf'),
        widget=CodeMirrorTextarea(
            mode="nginx", config={'fixedGutter': True}))


class EditBackupSettingsForm(forms.ModelForm):
    class Meta:
        model = BackupSettings
        fields = [
            'cron_time_for_db', 'keep_db_files',
            'cron_time_for_fs', 'keep_fs_files',
        ]


class AddWebApplicationForm(forms.ModelForm):
    docker_image = forms.ModelChoiceField(
        queryset=DockerImage.objects.filter(is_webapp=True),
        label=_(u'Docker image'))

    class Meta:
        model = WebApplication
        fields = ['short_description', 'name', 'host']

    def __init__(self, customer, *args, **kwargs):
        from hosts.models import Host
        super(AddWebApplicationForm, self).__init__(*args, **kwargs)
        self.customer = customer
        self.fields['host'].queryset = Host.objects.all()

        try:
            max_app_id = \
                WebApplication.objects.order_by('-id').first().id + 1
        except AttributeError:
            max_app_id = 1
        self.fields['name'].initial = 'webapp_{0:02d}'.format(max_app_id)


class AddDomainForm(forms.ModelForm):
    class Meta:
        model = Domain
        fields = ['domain', 'protocol']
        # widgets = {
        #     'applications': Select2MultipleWidget(),
        # }

    def __init__(self, webapp, *args, **kwargs):
        super(AddDomainForm, self).__init__(*args, **kwargs)


class AddLinkedFTPForm(forms.ModelForm):
    class Meta:
        model = LinkedFTP
        fields = ['ftpuser', 'directory', 'ftp_directory']

    def __init__(self, webapp, *args, **kwargs):
        from hosts.models import FTPUser
        super(AddLinkedFTPForm, self).__init__(*args, **kwargs)
        self.webapp = webapp
        self.fields['ftpuser'].queryset = \
            FTPUser.objects.filter(
                owner=webapp.owner)


class BackupToFTPLinkForm(forms.ModelForm):
    class Meta:
        model = BackupToFTPLink
        fields = ['ftpuser', 'ftp_directory']

    def __init__(self, webapp, *args, **kwargs):
        from hosts.models import FTPUser
        super(BackupToFTPLinkForm, self).__init__(*args, **kwargs)
        self.webapp = webapp
        self.fields['ftpuser'].queryset = \
            FTPUser.objects.filter(
                owner=webapp.owner)


class AddPostgreSQLUserForm(forms.ModelForm):
    class Meta:
        model = PostgreSQLUser
        fields = [
            'pg_service',
            'username', 'password']
        widgets = {
            'password': forms.PasswordInput(),
        }

    def __init__(self, customer, host, *args, **kwargs):
        super(AddPostgreSQLUserForm, self).__init__(*args, **kwargs)
        self.host = host
        self.customer = customer
        self.pg_services = self.host.postgresql_servers.all()
        self.fields['pg_service'].queryset = self.pg_services
        self.fields['pg_service'].initial = \
            self.pg_services.get(is_default=True)

    def clean_username(self):
        already_exist = PostgreSQLUser.objects.filter(
            pg_service=self.pg_services,
            username=self.cleaned_data['username']).exists()

        if not already_exist:
            return self.cleaned_data['username']
        raise forms.ValidationError(_(u'User already exists.'))


class AddPostgreSQLDatabaseForm(forms.ModelForm):
    pg_user = forms.ModelChoiceField(
        label=_(u'User name'), queryset=PostgreSQLUser.objects.none())

    class Meta:
        model = PostgreSQLDatabase
        fields = ['name']

    def __init__(self, customer, host, *args, **kwargs):
        super(AddPostgreSQLDatabaseForm, self).__init__(*args, **kwargs)
        self.host = host
        self.customer = customer
        self.pg_services = self.host.postgresql_servers.all()
        self.fields['pg_user'].queryset = PostgreSQLUser.objects.filter(
            pg_service=self.pg_services,
            owner=customer,
        )

    def clean_name(self):
        already_exist = PostgreSQLDatabase.objects.filter(
            pg_service=self.pg_services,
            name=self.cleaned_data['name']).exists()

        if not already_exist:
            return self.cleaned_data['name']
        raise forms.ValidationError(_(u'Database already exists.'))
