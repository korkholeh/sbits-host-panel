# coding: utf-8
from StringIO import StringIO
from django.conf import settings
from fabric.api import env
from fabric.api import sudo, run
from fabric.api import get
from fabric.contrib.files import upload_template
import requests
import os
import json


def add_linked_ftp(linked_ftp):
    host_obj = linked_ftp.webapp.host
    webapp = linked_ftp.webapp
    app_dir = webapp.docker_container.get_meta_settings(
        'Volumes./home/app')
    host_string = "%s@%s" % (
        host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    env.password = host_obj.sudo_password

    ftp_dir = os.path.join(
        '/home/{0}'.format(linked_ftp.ftpuser.username),
        linked_ftp.ftp_directory)
    sudo('mkdir -p %s' % ftp_dir)
    sudo('mount --bind {linkeddir} {ftpdir}'.format(
        linkeddir=os.path.join(app_dir, linked_ftp.directory),
        ftpdir=ftp_dir))


def remove_linked_ftp(linked_ftp):
    host_obj = linked_ftp.webapp.host
    host_string = "%s@%s" % (
        host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    env.password = host_obj.sudo_password

    ftp_dir = os.path.join(
        '/home/{0}'.format(linked_ftp.ftpuser.username),
        linked_ftp.ftp_directory)
    sudo('umount %s' % ftp_dir)
    sudo('rm -rf %s' % ftp_dir)


def add_backup_ftp(backup_ftp):
    host_obj = backup_ftp.backup_settings.webapp.host

    backup_dir = backup_ftp.backup_settings.directory
    host_string = "%s@%s" % (
        host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    env.password = host_obj.sudo_password

    ftp_dir = os.path.join(
        '/home/{0}'.format(backup_ftp.ftpuser.username),
        backup_ftp.ftp_directory)
    sudo('mkdir -p %s' % ftp_dir)
    sudo('mount --bind {linkeddir} {ftpdir}'.format(
        linkeddir=backup_dir,
        ftpdir=ftp_dir))


def remove_backup_ftp(backup_ftp):
    host_obj = backup_ftp.backup_settings.webapp.host
    host_string = "%s@%s" % (
        host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    env.password = host_obj.sudo_password

    ftp_dir = os.path.join(
        '/home/{0}'.format(backup_ftp.ftpuser.username),
        backup_ftp.ftp_directory)
    sudo('umount %s' % ftp_dir)
    sudo('rm -rf %s' % ftp_dir)


def get_file_list(host_obj, dir):
    host_string = "%s@%s" % (
        host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    env.password = host_obj.sudo_password
    return run('ls %s' % dir).split()


def sync_backup_settings(backup_settings):
    from .models import PostgreSQLUser
    import os

    host_obj = backup_settings.webapp.host
    host_string = "%s@%s" % (
        host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    env.password = host_obj.sudo_password
    run('mkdir -p %s' % backup_settings.directory)

    dbs = backup_settings.webapp.pg_databases.all()
    db_script_template = os.path.join(
        settings.FABRIC_TEMPLATE_DIR, 'backup_db.sh')
    db_script_destination = os.path.join(
        backup_settings.directory, 'backup_db.sh')

    if dbs:
        for db in dbs:
            db.pg_user = PostgreSQLUser.objects.get(
                username=db.username, pg_service=db.pg_service)

        upload_template(
            filename='backup_db.sh',
            destination=db_script_destination,
            context={
                'dbs': dbs,
                'backup_settings': backup_settings,
            },
            use_jinja=True,
            template_dir=settings.FABRIC_TEMPLATE_DIR,
        )
        # run(_script)
        run('chmod +x %s' % db_script_destination)

    fs_script_destination = os.path.join(
        backup_settings.directory, 'backup_fs.sh')
    upload_template(
        filename='backup_fs.sh',
        destination=fs_script_destination,
        context={
            'backup_settings': backup_settings,
        },
        use_jinja=True,
        template_dir=settings.FABRIC_TEMPLATE_DIR,
    )
    run('chmod +x %s' % fs_script_destination)


def get_server_config(webapp_obj):
    host_obj = webapp_obj.host
    host_string = "%s@%s" % (
        host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    env.password = host_obj.sudo_password

    config_dir = webapp_obj.docker_container.get_meta_settings(
        'Volumes./etc/nginx/sites-enabled')
    config_file = os.path.join(config_dir, 'webapp.conf')
    return sudo('cat %s' % config_file).replace('\r', '')


def save_config_file(webapp_obj, config, auth_user=None, auth_pwd=None):
    host_obj = webapp_obj.host
    host_string = "%s@%s" % (
        host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    env.password = host_obj.sudo_password

    config_dir = webapp_obj.docker_container.get_meta_settings(
        'Volumes./etc/nginx/sites-enabled')
    config_file = os.path.join(config_dir, 'webapp.conf')
    upload_template(
        filename='blank.conf',
        destination=config_file,
        context={
            'config': config.replace('\r', ''),
        },
        use_jinja=True,
        use_sudo=True,
        template_dir=settings.FABRIC_TEMPLATE_DIR,
    )
    if auth_user is not None and auth_pwd is not None:
        upload_template(
            filename='blank.conf',
            destination=os.path.join(config_dir, '.htpasswd'),
            context={
                'config': get_htpasswd_content(auth_user, auth_pwd),
            },
            use_jinja=True,
            use_sudo=True,
            template_dir=settings.FABRIC_TEMPLATE_DIR,
        )
    restart_service(webapp_obj, 'nginx')


def restart_service(webapp_obj, service):
    host_string = "%s@%s:%s" % (
        'root',
        webapp_obj.host.ip_address,
        webapp_obj.public_ssh_port)
    env.host_string = host_string
    run('service %s restart' % service)


def generate_password_hash(password):
    import base64
    import hashlib
    salt = os.urandom(8)  # edit the length as you see fit
    return '{SSHA}' + base64.b64encode(
        hashlib.sha1(password.encode('utf-8') + salt).digest() + salt)


def get_htpasswd_content(user, password):
    return '%s:%s' % (user, generate_password_hash(password))
