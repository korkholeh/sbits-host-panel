# coding: utf-8
from django.shortcuts import render_to_response, redirect, render
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.http import HttpResponseForbidden, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.contrib.auth.decorators import permission_required
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView
from django.views.generic import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView

from containers.models import (
    ContainerSSHKey,
)
from containers.forms import (
    AddContainerSSHKeyForm,
)
from .models import (
    WebApplication,
    Domain,
    LinkedFTP,
    PostgreSQLUser,
    PostgreSQLDatabase,
    ServerSettings,
    BackupToFTPLink,
)
from .forms import (
    AddWebApplicationForm,
    AddDomainForm,
    AddLinkedFTPForm,
    AddPostgreSQLUserForm,
    AddPostgreSQLDatabaseForm,
    EditBackupSettingsForm,
    EditServerConfigFileForm,
    EasyConfigForm,
    BackupToFTPLinkForm,
)


class WebApplicationListView(ListView):
    model = WebApplication
    context_object_name = 'webapps'


class WebApplicationDetailView(DetailView):
    model = WebApplication
    context_object_name = 'webapp'


class WebappSSHKeyListView(ListView):
    model = ContainerSSHKey
    context_object_name = 'keys'
    template_name = 'applications/webapp_sshkey_list.html'

    def get_queryset(self):
        self.webapp = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        return self.webapp.docker_container.sshkeys.all()

    def get_context_data(self, **kwargs):
        context = super(
            WebappSSHKeyListView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.webapp,
        })
        return context


class WebappSSHKeyCreateView(CreateView):
    model = ContainerSSHKey
    form_class = AddContainerSSHKeyForm
    template_name = 'applications/webapp_add_sshkey.html'

    def get_form_kwargs(self):
        kwargs = super(WebappSSHKeyCreateView, self).get_form_kwargs()
        self.webapp = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        kwargs['container'] = self.webapp.docker_container
        kwargs['customer'] = self.request.user
        return kwargs

    def form_valid(self, form):
        sshkey = form.save(commit=False)
        sshkey.docker_container = self.webapp.docker_container
        sshkey.added_by = self.request.user
        sshkey.save()
        self.webapp.docker_container.sync_sshkeys()
        return super(WebappSSHKeyCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(
            WebappSSHKeyCreateView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.webapp,
        })
        return context

    def get_success_url(self):
        return reverse('applications:webapp_sshkeys', args=(self.webapp.pk,))


class WebappSSHKeyDeleteView(DeleteView):
    model = ContainerSSHKey
    template_name = 'applications/webapp_sshkey_confirm_delete.html'

    def get_object(self):
        self.webapp = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        result = get_object_or_404(
            ContainerSSHKey,
            docker_container=self.webapp.docker_container,
            pk=self.kwargs.get('pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super(
            WebappSSHKeyDeleteView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.webapp,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.object.delete()
        self.webapp.docker_container.sync_sshkeys()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('applications:webapp_sshkeys', args=(self.webapp.pk,))


class WebApplicationCreateView(CreateView):
    model = WebApplication
    form_class = AddWebApplicationForm
    template_name = 'applications/add_webapp.html'

    def get_form_kwargs(self):
        kwargs = super(WebApplicationCreateView, self).get_form_kwargs()
        kwargs['customer'] = self.request.user
        return kwargs

    def form_valid(self, form):
        from containers.models import Container
        import random
        webapp = form.save(commit=False)
        docker_image = form.cleaned_data['docker_image']

        webapp.owner = self.request.user
        webapp.ide_user = 'ideuser'
        webapp.ide_password = ''.join(
            random.choice('qwertyuiopasdfghjklzxcvbnm') for _ in range(6))

        _container = Container(
            name=webapp.name,
            host=webapp.host,
            docker_image=docker_image)
        _container.save()
        _container.run(env_variables={
            'IDE_USER': webapp.ide_user,
            'IDE_PASSWORD': webapp.ide_password,
        })
        _container.sync_sshkeys()

        webapp.docker_container = _container
        webapp.public_app_port = \
            _container.get_port(name='webservice')
        webapp.public_ssh_port = \
            _container.get_port(name='ssh')
        webapp.ide_port = \
            _container.get_port(name='ide')
        webapp.save()

        domain = Domain(
            domain='%s.%s' % (webapp.name, webapp.host.host_domain),
            protocol='http')
        domain.webapp = webapp
        domain.hipache = webapp.host.hipache
        domain.added_by = self.request.user
        domain.is_default = True
        domain.save()
        domain.webapp.sync_domains()
        self.webapp = webapp

        server_settings = ServerSettings(
            webapp=webapp,
        )
        server_settings.save()
        server_settings.compile_config()
        return super(WebApplicationCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('applications:webapp', args=(self.webapp.pk,))


class WebApplicationDeleteView(DeleteView):
    model = WebApplication
    context_object_name = 'webapp'
    template_name = 'applications/webapplication_confirm_delete.html'

    def get_object(self):
        result = get_object_or_404(
            WebApplication,
            pk=self.kwargs.get('pk'))
        self.webapp = result
        return result

    def get_context_data(self, **kwargs):
        context = super(
            WebApplicationDeleteView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.webapp,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.object.docker_container.destroy()
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('applications:application_list')


def databases(request, app_pk):
    # user = request.user
    webapp = get_object_or_404(WebApplication, pk=app_pk)

    pg_users = webapp.pg_users.all()
    pg_databases = webapp.pg_databases.all()
    return render(
        request,
        'applications/databases.html',
        {
            'webapp': webapp,
            'pg_users': pg_users,
            'pg_databases': pg_databases,
        })


class DatabaseUserCreateView(CreateView):
    model = PostgreSQLUser
    form_class = AddPostgreSQLUserForm
    template_name = 'applications/webapp_add_pg_user.html'

    def get_form_kwargs(self):
        kwargs = super(DatabaseUserCreateView, self).get_form_kwargs()
        self.webapp = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        kwargs['customer'] = self.request.user
        kwargs['host'] = self.webapp.host
        return kwargs

    def form_valid(self, form):
        pg_user = form.save(commit=False)
        pg_user.webapp = self.webapp
        pg_user.owner = self.request.user
        pg_user.save()
        pg_user.add_user_to_server()
        return super(DatabaseUserCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(
            DatabaseUserCreateView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.webapp,
        })
        return context

    def get_success_url(self):
        return reverse('applications:webapp_databases', args=(self.webapp.pk,))


class DatabaseUserDeleteView(DeleteView):
    model = PostgreSQLUser
    template_name = 'applications/webapp_pg_user_confirm_delete.html'

    def get_object(self):
        result = get_object_or_404(
            PostgreSQLUser,
            webapp=self.kwargs.get('app_pk'),
            pk=self.kwargs.get('pk'))
        self.pg_user = result
        return result

    def get_context_data(self, **kwargs):
        context = super(
            DatabaseUserDeleteView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.pg_user.webapp,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.object.remove_user_from_server()
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse(
            'applications:webapp_databases', args=(self.pg_user.webapp.pk,))


class DatabaseCreateView(CreateView):
    model = PostgreSQLDatabase
    form_class = AddPostgreSQLDatabaseForm
    template_name = 'applications/webapp_add_pg_db.html'

    def get_form_kwargs(self):
        kwargs = super(DatabaseCreateView, self).get_form_kwargs()
        self.webapp = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        kwargs['customer'] = self.request.user
        kwargs['host'] = self.webapp.host
        return kwargs

    def form_valid(self, form):
        pg_db = form.save(commit=False)

        pg_user = get_object_or_404(
            PostgreSQLUser,
            owner=self.request.user,
            webapp=self.webapp,
            username=form.cleaned_data['pg_user'].username)

        pg_db.webapp = self.webapp
        pg_db.owner = self.request.user
        pg_db.pg_service = pg_user.pg_service
        pg_db.username = pg_user.username
        pg_db.save()
        pg_db.add_db_to_server()
        return super(DatabaseCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(
            DatabaseCreateView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.webapp,
        })
        return context

    def get_success_url(self):
        return reverse('applications:webapp_databases', args=(self.webapp.pk,))


class DatabaseDeleteView(DeleteView):
    model = PostgreSQLDatabase
    template_name = 'applications/webapp_pg_db_confirm_delete.html'

    def get_object(self):
        result = get_object_or_404(
            PostgreSQLDatabase,
            webapp=self.kwargs.get('app_pk'),
            pk=self.kwargs.get('pk'))
        self.pg_db = result
        return result

    def get_context_data(self, **kwargs):
        context = super(
            DatabaseDeleteView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.pg_db.webapp,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.object.remove_db_from_server()
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse(
            'applications:webapp_databases', args=(self.pg_db.webapp.pk,))


def pg_connection(request, app_pk, pk):
    user = request.user
    webapp = get_object_or_404(WebApplication, pk=app_pk)
    pg_db = get_object_or_404(PostgreSQLDatabase, webapp=webapp, pk=pk)
    postgresql_service = pg_db.pg_service
    pg_user = PostgreSQLUser.objects.get(
        # owner=user,
        username=pg_db.username,
        pg_service=postgresql_service)

    django_connection = """DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '%s',
        'USER': '%s',
        'PASSWORD': '%s',
        'HOST': '%s',
        'PORT': '%s',
    },
}
    """ % (
        pg_db.name,
        pg_db.username,
        pg_user.password,
        postgresql_service.host.ip_address,
        postgresql_service.public_service_port,
    )

    flask_connection = \
        "SQLALCHEMY_DATABASE_URI = 'postgresql://%s:%s@%s:%s/%s'" % (
        pg_db.username,
        pg_user.password,
        postgresql_service.host.ip_address,
        postgresql_service.public_service_port,
        pg_db.name,
        )

    psql_connection = \
        'psql "dbname=%s host=%s user=%s password=%s port=%s"' % (
        pg_db.name,
        postgresql_service.host.ip_address,
        pg_db.username,
        pg_user.password,
        postgresql_service.public_service_port,
        )

    return render_to_response(
        'applications/postgresql_connection.html',
        RequestContext(request, {
            'webapp': webapp,
            'postgresql_service': postgresql_service,
            'pg_db': pg_db,
            'django_connection': django_connection,
            'flask_connection': flask_connection,
            'psql_connection': psql_connection,
        }))


def pg_dumps(request, app_pk, pk):
    import datetime
    user = request.user
    webapp = get_object_or_404(WebApplication, pk=app_pk)
    pg_db = get_object_or_404(PostgreSQLDatabase, webapp=webapp, pk=pk)
    postgresql_service = pg_db.pg_service
    pg_user = PostgreSQLUser.objects.get(
        # owner=user,
        username=pg_db.username, pg_service=postgresql_service)

    db_sql_dump = \
        'pg_dump "dbname=%s host=%s user=%s password=%s port=%s"' \
        ' > dump-%s.sql' % (
        pg_db.name,
        postgresql_service.host.ip_address,
        pg_db.username,
        pg_user.password,
        postgresql_service.public_service_port,
        datetime.date.today(),
        )

    db_restore_sql = \
        'cat db_dump.sql |' \
        ' psql "dbname=%s host=%s user=%s password=%s port=%s"' % (
        pg_db.name,
        postgresql_service.host.ip_address,
        pg_db.username,
        pg_user.password,
        postgresql_service.public_service_port,
        )

    db_compressed_dump = \
        'pg_dump "dbname=%s host=%s user=%s password=%s port=%s"' \
        ' | gzip > dump-%s.gz' % (
        pg_db.name,
        postgresql_service.host.ip_address,
        pg_db.username,
        pg_user.password,
        postgresql_service.public_service_port,
        datetime.date.today(),
        )

    db_restore_compressed = \
        'gunzip -c db_dump.gz |' \
        ' psql "dbname=%s host=%s user=%s password=%s port=%s"' % (
        pg_db.name,
        postgresql_service.host.ip_address,
        pg_db.username,
        pg_user.password,
        postgresql_service.public_service_port,
        )

    return render_to_response(
        'applications/postgresql_dumps.html',
        RequestContext(request, {
            'webapp': webapp,
            'postgresql_service': postgresql_service,
            'pg_db': pg_db,
            'db_sql_dump': db_sql_dump,
            'db_restore_sql': db_restore_sql,
            'db_compressed_dump': db_compressed_dump,
            'db_restore_compressed': db_restore_compressed,
        }))


def files(request, app_pk):
    # user = request.user
    webapp = get_object_or_404(WebApplication, pk=app_pk)
    ide_link = ''
    if webapp.ide_port:
        ide_link = 'http://{host}:{port}'.format(
            host=webapp.host.ip_address,
            port=webapp.ide_port,
        )
    return render(
        request, 'applications/webapp_files.html',
        {
            'webapp': webapp,
            'ide_link': ide_link,
        })


class LinkedFTPListView(ListView):
    model = LinkedFTP
    context_object_name = 'linked_ftps'

    def get_queryset(self):
        self.webapp = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        return LinkedFTP.objects.filter(webapp=self.webapp)

    def get_context_data(self, **kwargs):
        context = super(
            LinkedFTPListView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.webapp,
        })
        return context


class LinkedFTPCreateView(CreateView):
    model = LinkedFTP
    form_class = AddLinkedFTPForm

    def get_form_kwargs(self):
        kwargs = super(LinkedFTPCreateView, self).get_form_kwargs()
        self.webapp = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        kwargs['webapp'] = self.webapp
        return kwargs

    def form_valid(self, form):
        linked_ftp = form.save(commit=False)
        linked_ftp.webapp = self.webapp
        linked_ftp.added_by = self.request.user
        linked_ftp.save()
        linked_ftp.add_to_server()
        return super(LinkedFTPCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(
            LinkedFTPCreateView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.webapp,
        })
        return context

    def get_success_url(self):
        return reverse(
            'applications:webapp_linkedftps', args=(self.webapp.pk,))


class LinkedFTPDeleteView(DeleteView):
    model = LinkedFTP

    def get_object(self):
        result = get_object_or_404(
            LinkedFTP,
            webapp=self.kwargs.get('app_pk'),
            pk=self.kwargs.get('pk'))
        self.pg_db = result
        return result

    def get_context_data(self, **kwargs):
        context = super(
            LinkedFTPDeleteView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.pg_db.webapp,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.object.remove_from_server()
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse(
            'applications:webapp_linkedftps', args=(self.pg_db.webapp.pk,))


class DomainListView(ListView):
    model = Domain
    context_object_name = 'domains'

    def get_queryset(self):
        self.webapp = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        return Domain.objects.filter(
            webapp=self.webapp).order_by('-is_default')

    def get_context_data(self, **kwargs):
        context = super(
            DomainListView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.webapp,
        })
        return context


class DomainCreateView(CreateView):
    model = Domain
    form_class = AddDomainForm

    def get_form_kwargs(self):
        kwargs = super(DomainCreateView, self).get_form_kwargs()
        self.webapp = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        kwargs['webapp'] = self.webapp
        return kwargs

    def form_valid(self, form):
        domains_exists = self.webapp.domains.count() > 0

        domain = form.save(commit=False)
        domain.webapp = self.webapp
        domain.hipache = self.webapp.host.hipache
        domain.added_by = self.request.user
        domain.is_default = not domains_exists
        domain.save()
        domain.webapp.sync_domains()
        return super(DomainCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(
            DomainCreateView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.webapp,
        })
        return context

    def get_success_url(self):
        return reverse(
            'applications:webapp_domains', args=(self.webapp.pk,))


class DomainDeleteView(DeleteView):
    model = Domain

    def get_object(self):
        result = get_object_or_404(
            Domain,
            webapp=self.kwargs.get('app_pk'),
            pk=self.kwargs.get('pk'))
        self.domain = result
        return result

    def get_context_data(self, **kwargs):
        context = super(
            DomainDeleteView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.domain.webapp,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.object.webapp.remove_domains()
        self.object.delete()
        self.object.webapp.sync_domains()

        _domains = Domain.objects.filter(webapp=self.domain.webapp)
        _d_count = _domains.count()
        if _d_count > 0:
            try:
                _domains.get(is_default=True)
            except:
                _d = _domains.first()
                _d.is_default = True
                _d.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse(
            'applications:webapp_domains', args=(self.domain.webapp.pk,))


def set_default_domain(request, app_pk, pk):
    webapp = get_object_or_404(
        WebApplication, pk=app_pk)
    _all_domains = Domain.objects.filter(webapp=webapp)
    for _domain in _all_domains:
        if _domain.pk == int(pk):
            _domain.is_default = True
        else:
            _domain.is_default = False
        _domain.save()
    return redirect(reverse(
        'applications:webapp_domains', args=(webapp.pk,)))


def backups_view(request, app_pk):
    webapp = get_object_or_404(WebApplication, pk=app_pk)
    db_backups = []
    fs_backups = []
    ftps = []
    if hasattr(webapp, 'backup_settings'):
        db_backups = webapp.backup_settings.get_db_backup_list()
        fs_backups = webapp.backup_settings.get_fs_backup_list()
        ftps = webapp.backup_settings.linked_ftps.all()
    return render(
        request, 'applications/backups.html',
        {
            'webapp': webapp,
            'db_backups': db_backups,
            'fs_backups': fs_backups,
            'ftps': ftps,
        })


def configure_backups(request, app_pk):
    webapp = get_object_or_404(WebApplication, pk=app_pk)

    if hasattr(webapp, 'backup_settings'):
        form = EditBackupSettingsForm(instance=webapp.backup_settings)
    else:
        form = EditBackupSettingsForm()

    if request.method == 'POST':
        if hasattr(webapp, 'backup_settings'):
            form = EditBackupSettingsForm(
                data=request.POST,
                instance=webapp.backup_settings)
        else:
            form = EditBackupSettingsForm(request.POST)

        if form.is_valid():
            if hasattr(webapp, 'backup_settings'):
                _bs = form.save()
            else:
                _bs = form.save(commit=False)
                _bs.webapp = webapp
                _bs.directory = '/home/webprod/backups/%s' % webapp.name
                _bs.save()
            _bs.sync_backup_settings()
            return redirect(reverse(
                'applications:webapp_backups', args=(webapp.pk,)))

    return render(
        request, 'applications/configure_backups.html',
        {
            'webapp': webapp,
            'form': form,
        })


class BackupToFTPLinkCreateView(CreateView):
    model = BackupToFTPLink
    form_class = BackupToFTPLinkForm

    def get_form_kwargs(self):
        kwargs = super(BackupToFTPLinkCreateView, self).get_form_kwargs()
        self.webapp = get_object_or_404(
            WebApplication, pk=self.kwargs.get('app_pk'))
        kwargs['webapp'] = self.webapp
        return kwargs

    def form_valid(self, form):
        _ftp = form.save(commit=False)
        _ftp.backup_settings = self.webapp.backup_settings
        _ftp.added_by = self.request.user
        _ftp.save()
        _ftp.add_to_server()
        return super(BackupToFTPLinkCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(
            BackupToFTPLinkCreateView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.webapp,
        })
        return context

    def get_success_url(self):
        return reverse(
            'applications:webapp_backups', args=(self.webapp.pk,))


class BackupToFTPLinkDeleteView(DeleteView):
    model = BackupToFTPLink

    def get_object(self):
        result = get_object_or_404(
            BackupToFTPLink,
            backup_settings__webapp=self.kwargs.get('app_pk'),
            pk=self.kwargs.get('pk'))
        self.ftp = result
        return result

    def get_context_data(self, **kwargs):
        context = super(
            BackupToFTPLinkDeleteView, self).get_context_data(**kwargs)
        context.update({
            'webapp': self.ftp.backup_settings.webapp,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.object.remove_from_server()
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse(
            'applications:webapp_backups',
            args=(self.ftp.backup_settings.webapp.pk,))


def change_settings_mode(request, app_pk):
    webapp = get_object_or_404(WebApplication, pk=app_pk)
    mode = request.GET.get('mode', 'easy')
    server_settings = webapp.server_settings
    server_settings.manual_config = mode == 'manual'
    server_settings.save()
    return redirect(reverse(
        'applications:webapp_settings', args=(webapp.pk,)))


def restart_nginx(request, app_pk):
    from .utils import restart_service
    webapp = get_object_or_404(WebApplication, pk=app_pk)
    restart_service(webapp, 'nginx')
    return redirect(reverse(
        'applications:webapp_settings', args=(webapp.pk,)))


def settings(request, app_pk):
    # user = request.user
    webapp = get_object_or_404(WebApplication, pk=app_pk)
    if not hasattr(webapp, 'server_settings'):
        server_settings = ServerSettings(webapp=webapp)
        server_settings.save()
    server_settings = webapp.server_settings

    main_domain = webapp.domains.get(is_default=True)
    extra_domains = webapp.domains.exclude(pk=main_domain.pk)

    if server_settings.manual_config:
        form = EditServerConfigFileForm(initial={
            'config': server_settings.get_config_file(),
        })
        if request.method == 'POST':
            form = EditServerConfigFileForm(request.POST)
            if form.is_valid():
                server_settings.save_config_file(form.cleaned_data['config'])
    else:
        form = EasyConfigForm(instance=server_settings)
        if request.method == 'POST':
            form = EasyConfigForm(
                instance=server_settings,
                data=request.POST)
            if form.is_valid():
                server_settings = form.save()
                server_settings.compile_config()

    return render(
        request, 'applications/settings.html',
        {
            'webapp': webapp,
            'server_settings': server_settings,
            'form': form,
            'main_domain': main_domain,
            'extra_domains': extra_domains,
        })
