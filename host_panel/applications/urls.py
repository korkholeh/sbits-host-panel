# coding: utf-8
from django.conf.urls import patterns, url
from .views import (
    WebApplicationListView,
    WebApplicationCreateView,
    WebApplicationDetailView,
    WebApplicationDeleteView,
    WebappSSHKeyListView,
    WebappSSHKeyCreateView,
    WebappSSHKeyDeleteView,
    DatabaseUserCreateView,
    DatabaseUserDeleteView,
    DatabaseCreateView,
    DatabaseDeleteView,
    LinkedFTPListView,
    LinkedFTPCreateView,
    LinkedFTPDeleteView,
    DomainListView,
    DomainCreateView,
    DomainDeleteView,
    BackupToFTPLinkCreateView,
    BackupToFTPLinkDeleteView,
)


urlpatterns = patterns(
    'applications.views',
    url(
        r'^(?P<app_pk>\d+)/linkedftps/add/$',
        LinkedFTPCreateView.as_view(), name='webapp_add_linkedftp'),
    url(
        r'^(?P<app_pk>\d+)/linkedftps/(?P<pk>\d+)/remove/$',
        LinkedFTPDeleteView.as_view(), name='webapp_remove_linkedftp'),
    url(
        r'^(?P<app_pk>\d+)/linkedftps/$',
        LinkedFTPListView.as_view(), name='webapp_linkedftps'),


    url(
        r'^(?P<app_pk>\d+)/sshkeys/$',
        WebappSSHKeyListView.as_view(), name='webapp_sshkeys'),
    url(
        r'^(?P<app_pk>\d+)/sshkeys/add/$',
        WebappSSHKeyCreateView.as_view(),
        name='webapp_add_sshkey'),
    url(
        r'^(?P<app_pk>\d+)/sshkeys/(?P<pk>\d+)/remove/$',
        WebappSSHKeyDeleteView.as_view(),
        name='webapp_remove_sshkey'),

    url(
        r'^(?P<app_pk>\d+)/domains/$',
        DomainListView.as_view(), name='webapp_domains'),
    url(
        r'^(?P<app_pk>\d+)/domains/(?P<pk>\d+)/remove/$',
        DomainDeleteView.as_view(), name='webapp_remove_domain'),
    url(
        r'^(?P<app_pk>\d+)/domains/(?P<pk>\d+)/set-default/$',
        'set_default_domain', name='webapp_set_default_domain'),
    url(
        r'^(?P<app_pk>\d+)/domains/add/$',
        DomainCreateView.as_view(), name='webapp_add_domain'),

    url(r'^(?P<app_pk>\d+)/files/$', 'files', name='webapp_files'),

    url(
        r'^(?P<app_pk>\d+)/databases/pgdbs/add/$',
        DatabaseCreateView.as_view(), name='webapp_add_pgdb'),
    url(
        r'^(?P<app_pk>\d+)/databases/pgdbs/(?P<pk>\d+)/remove/$',
        DatabaseDeleteView.as_view(), name='webapp_remove_pgdb'),
    url(
        r'^(?P<app_pk>\d+)/databases/pgdbs/(?P<pk>\d+)/connection/$',
        'pg_connection', name='webapp_pgconnection'),
    url(
        r'^(?P<app_pk>\d+)/databases/pgdbs/(?P<pk>\d+)/dumps/$',
        'pg_dumps', name='webapp_pgdumps'),

    url(
        r'^(?P<app_pk>\d+)/databases/pgusers/(?P<pk>\d+)/remove/$',
        DatabaseUserDeleteView.as_view(), name='webapp_remove_pguser'),
    url(
        r'^(?P<app_pk>\d+)/databases/pgusers/add/$',
        DatabaseUserCreateView.as_view(), name='webapp_add_pguser'),
    url(
        r'^(?P<app_pk>\d+)/databases/$',
        'databases', name='webapp_databases'),

    url(
        r'^(?P<pk>\d+)/remove/$',
        WebApplicationDeleteView.as_view(), name='remove_webapp'),
    url(
        r'^(?P<pk>\d+)/$',
        WebApplicationDetailView.as_view(), name='webapp'),
    url(
        r'^add/$',
        WebApplicationCreateView.as_view(), name='add_webapp'),
    url(
        r'^$',
        WebApplicationListView.as_view(), name='application_list'),

    url(
        r'^(?P<app_pk>\d+)/backups/$',
        'backups_view', name='webapp_backups'),
    url(
        r'^(?P<app_pk>\d+)/backups/ftp-links/add/$',
        BackupToFTPLinkCreateView.as_view(), name='webapp_add_backup_ftp'),
    url(
        r'^(?P<app_pk>\d+)/backups/ftp-links/(?P<pk>\d+)/remove/$',
        BackupToFTPLinkDeleteView.as_view(), name='webapp_remove_backup_ftp'),
    url(
        r'^(?P<app_pk>\d+)/backups/edit/$',
        'configure_backups', name='webapp_configure_backups'),

    url(
        r'^(?P<app_pk>\d+)/settings/change-mode/$',
        'change_settings_mode', name='webapp_settings_mode'),
    url(
        r'^(?P<app_pk>\d+)/settings/restart-nginx/$',
        'restart_nginx', name='webapp_restart_nginx'),
    url(
        r'^(?P<app_pk>\d+)/settings/$',
        'settings', name='webapp_settings'),
)
