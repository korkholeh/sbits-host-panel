# encoding: utf8
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0005_linkedftp'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('services', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Domain',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('domain', models.CharField(unique=True, max_length=100, verbose_name=u'Domain')),
                ('protocol', models.CharField(default='http', max_length=20, verbose_name=u'Protocol', choices=[('http', 'HTTP')])),
                ('webapp', models.ForeignKey(to='applications.WebApplication', to_field=u'id', verbose_name=u'Applications')),
                ('hipache', models.ForeignKey(to_field=u'id', blank=True, to='services.Hipache', null=True)),
                ('is_default', models.BooleanField(default=False, help_text=u'Only one domain for app can be default.', verbose_name=u'Is default')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Added by')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
            ],
            options={
                u'verbose_name': u'Domain',
                u'verbose_name_plural': u'Domains',
            },
            bases=(models.Model,),
        ),
    ]
