# encoding: utf8
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    replaces = [('applications', '0001_initial'), ('applications', '0002_serversettings'), ('applications', '0003_backupsettings'), ('applications', '0004_backuptoftplink'), ('applications', '0005_linkedftp'), ('applications', '0006_domain'), ('applications', '0007_postgresqldatabase'), ('applications', '0008_postgresqluser')]

    dependencies = [
        ('containers', '__first__'),
        ('hosts', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('services', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='WebApplication',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('short_description', models.CharField(max_length=300, verbose_name=u'Short description')),
                ('name', models.CharField(unique=True, max_length=100, verbose_name=u'Name')),
                ('host', models.ForeignKey(to='hosts.Host', to_field=u'id', verbose_name=u'Host')),
                ('docker_container', models.OneToOneField(null=True, to_field=u'id', blank=True, to='containers.Container', verbose_name=u'Container')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Owner')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
                ('public_app_port', models.PositiveIntegerField(verbose_name=u'Public app port')),
                ('public_ssh_port', models.PositiveIntegerField(null=True, verbose_name=u'Public SSH port', blank=True)),
                ('ide_port', models.PositiveIntegerField(null=True, verbose_name=u'IDE port', blank=True)),
                ('ide_user', models.CharField(max_length=100, verbose_name=u'IDE user', blank=True)),
                ('ide_password', models.CharField(max_length=100, verbose_name=u'IDE password', blank=True)),
            ],
            options={
                u'ordering': ['short_description'],
                u'verbose_name': u'Web application',
                u'verbose_name_plural': u'Web applications',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ServerSettings',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('webapp', models.OneToOneField(verbose_name=u'Web application', to_field=u'id', to='applications.WebApplication')),
                ('manual_config', models.BooleanField(default=False, verbose_name=u'Manual config')),
                ('public_root', models.CharField(default='/home/app/webapp/public', max_length=255, verbose_name=u'Project public root')),
                ('passenger_app_root', models.CharField(default='', max_length=255, verbose_name=u'Application root', blank=True)),
                ('extra_top', models.TextField(default='', verbose_name=u'Extra top config', blank=True)),
                ('set_real_ip_from', models.CharField(default='172.17.42.1', max_length=255, verbose_name=u'Set real IP from', blank=True)),
                ('real_ip_header', models.CharField(default='X-Forwarded-For', max_length=255, verbose_name=u'Real IP header', blank=True)),
                ('passenger_enabled', models.BooleanField(default=True, verbose_name=u'Passenger enabled')),
                ('passenger_user', models.CharField(default='app', max_length=100, verbose_name=u'Application user')),
                ('passenger_friendly_error_pages', models.BooleanField(default=False, verbose_name=u'Passenger friendly error pages')),
                ('passenger_python', models.CharField(default='/usr/bin/python2.7', max_length=100, verbose_name=u'Python version', choices=[('/usr/bin/python2.7', '2.7'), ('/usr/bin/python3', '3')])),
                ('extra_tail', models.TextField(default='', verbose_name=u'Extra tail config', blank=True)),
                ('auth_enabled', models.BooleanField(default=False, verbose_name=u'Auth enabled')),
                ('auth_user', models.CharField(default='dev', max_length=100, verbose_name=u'Auth user')),
                ('auth_password', models.CharField(default='dev', max_length=100, verbose_name=u'Auth password')),
                ('auth_exlude_ips', models.TextField(default='', help_text=u'One IP per line.', verbose_name=u'Auth exclude IPs', blank=True)),
                ('passenger_max_pool_size', models.PositiveIntegerField(help_text=u'The maximum number of application processes that may simultanously exist.', null=True, verbose_name=u'Max pool size', blank=True)),
                ('passenger_min_instances', models.PositiveIntegerField(help_text=u'This specifies the minimum number of application processes that should exist for a given application.', null=True, verbose_name=u'Min instances', blank=True)),
                ('passenger_pool_idle_time', models.PositiveIntegerField(help_text=u'The maximum number of seconds that an application process may be idle.', null=True, verbose_name=u'Pool idle time', blank=True)),
                ('passenger_max_preloader_idle_time', models.PositiveIntegerField(help_text=u'The preloader process has an idle timeout, just like the backend processes spawned by Phusion Passenger do.', null=True, verbose_name=u'Max preloader idle time', blank=True)),
                ('passenger_start_timeout', models.PositiveIntegerField(help_text=u'Specifies a timeout for the startup of application processes.', null=True, verbose_name=u'Start timeout', blank=True)),
                ('passenger_max_requests', models.PositiveIntegerField(help_text=u'The maximum number of requests an application process will process.', null=True, verbose_name=u'Max requests', blank=True)),
                ('passenger_pre_start', models.BooleanField(default=False, verbose_name=u'Passenger pre start')),
                ('passenger_max_request_queue_size', models.PositiveIntegerField(help_text=u'If the queue is already at this specified limit, then Phusion Passenger will immediately send a "503 Service Unavailable" error to any incoming requests.', null=True, verbose_name=u'Max request queue size', blank=True)),
                ('passenger_request_queue_overflow_status_code', models.PositiveIntegerField(help_text=u'This option allows you to customize the HTTP status code that is sent back when the request queue is full.', null=True, verbose_name=u'Request queue overflow status code', blank=True)),
                ('passenger_ignore_client_abort', models.BooleanField(default=False, verbose_name=u'Ignore client abort')),
                ('passenger_intercept_errors', models.BooleanField(default=False, verbose_name=u'Passenger intercept errors')),
                ('passenger_pass_header', models.CharField(default='', help_text=u'Some headers generated by backend applications are not forwarded to the HTTP client, e.g. X-Accel-Redirect which is directly processed by Nginx and then discarded from the final response.', max_length=255, verbose_name=u'Passenger pass header', blank=True)),
                ('passenger_buffer_response', models.BooleanField(default=False, verbose_name=u'Buffer response')),
                ('passenger_log_level', models.PositiveIntegerField(blank=True, null=True, verbose_name=u'Log level', choices=[(0, u'Show only errors and warnings'), (1, u'Show the most important debugging information'), (2, u'Show more debugging information'), (3, u'Show even more debugging information')])),
                ('passenger_debug_log_file', models.CharField(default='', help_text=u'By default Phusion Passenger debugging and error messages are written to the global web server error log. This option allows one to specify the file that debugging and error messages should be written to instead.', max_length=255, verbose_name=u'Debug log file', blank=True)),
            ],
            options={
                u'verbose_name': u'Server settings',
                u'verbose_name_plural': u'Server settings',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BackupSettings',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('webapp', models.OneToOneField(verbose_name=u'Web application', to_field=u'id', to='applications.WebApplication')),
                ('directory', models.CharField(default='/home/webprod/backups', max_length=400, verbose_name=u'Directory')),
                ('cron_time_for_db', models.CharField(default='0 0 * * *', help_text=u'minute (0-59), hour (0-23, 0 = midnight), day (1-31), month (1-12), weekday (0-6, 0 = Sunday)', max_length=200, verbose_name=u'Cron time for DB')),
                ('keep_db_files', models.PositiveIntegerField(default=5, help_text=u'In days', verbose_name=u'Keep DB files')),
                ('db_backup_task', models.ForeignKey(verbose_name=u'DB backup task', to_field=u'id', blank=True, to='hosts.CronTask', null=True)),
                ('cron_time_for_fs', models.CharField(default='0 0 * * *', help_text=u'minute (0-59), hour (0-23, 0 = midnight), day (1-31), month (1-12), weekday (0-6, 0 = Sunday)', max_length=200, verbose_name=u'Cron time for FS')),
                ('keep_fs_files', models.PositiveIntegerField(default=1, help_text=u'In days', verbose_name=u'Keep FS files')),
                ('fs_backup_task', models.ForeignKey(verbose_name=u'FS backup task', to_field=u'id', blank=True, to='hosts.CronTask', null=True)),
            ],
            options={
                u'unique_together': set([('webapp', 'directory')]),
                u'verbose_name': u'Backup settings',
                u'verbose_name_plural': u'Backup settings',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BackupToFTPLink',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('backup_settings', models.ForeignKey(to='applications.BackupSettings', to_field=u'id', verbose_name=u'Backup settings')),
                ('ftpuser', models.ForeignKey(to='hosts.FTPUser', to_field=u'id', verbose_name=u'FTP User')),
                ('ftp_directory', models.CharField(default='backups', max_length=400, verbose_name=u'FTP directory')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Added by')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
            ],
            options={
                u'unique_together': set([('backup_settings', 'ftpuser')]),
                u'verbose_name': u'Backup to FTP links',
                u'verbose_name_plural': u'Backup to FTP links',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LinkedFTP',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('webapp', models.ForeignKey(to='applications.WebApplication', to_field=u'id', verbose_name=u'Web application')),
                ('ftpuser', models.ForeignKey(to='hosts.FTPUser', to_field=u'id', verbose_name=u'FTP User')),
                ('directory', models.CharField(default='webapp/ftp', max_length=400, verbose_name=u'Directory')),
                ('ftp_directory', models.CharField(default='files', max_length=400, verbose_name=u'FTP directory')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Added by')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
            ],
            options={
                u'unique_together': set([('webapp', 'directory')]),
                u'verbose_name': u'Linked FTP',
                u'verbose_name_plural': u'Linked FTPs',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Domain',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('domain', models.CharField(unique=True, max_length=100, verbose_name=u'Domain')),
                ('protocol', models.CharField(default='http', max_length=20, verbose_name=u'Protocol', choices=[('http', 'HTTP')])),
                ('webapp', models.ForeignKey(to='applications.WebApplication', to_field=u'id', verbose_name=u'Applications')),
                ('hipache', models.ForeignKey(to_field=u'id', blank=True, to='services.Hipache', null=True)),
                ('is_default', models.BooleanField(default=False, help_text=u'Only one domain for app can be default.', verbose_name=u'Is default')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Added by')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
            ],
            options={
                u'verbose_name': u'Domain',
                u'verbose_name_plural': u'Domains',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PostgreSQLDatabase',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=u'Name')),
                ('username', models.CharField(max_length=100, verbose_name=u'User name')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Owner')),
                ('webapp', models.ForeignKey(to='applications.WebApplication', to_field=u'id', verbose_name=u'Web application')),
                ('pg_service', models.ForeignKey(to='services.PostgreSQLService', to_field=u'id', verbose_name=u'PG Service')),
            ],
            options={
                u'verbose_name': u'PostgreSQL Database',
                u'verbose_name_plural': u'PostgreSQL Databases',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PostgreSQLUser',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=100, verbose_name=u'User name')),
                ('password', models.CharField(max_length=100, verbose_name=u'Password')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Owner')),
                ('webapp', models.ForeignKey(to='applications.WebApplication', to_field=u'id', verbose_name=u'Web application')),
                ('pg_service', models.ForeignKey(to='services.PostgreSQLService', to_field=u'id', verbose_name=u'PG Service')),
            ],
            options={
                u'verbose_name': u'PostgreSQL User',
                u'verbose_name_plural': u'PostgreSQL Users',
            },
            bases=(models.Model,),
        ),
    ]
