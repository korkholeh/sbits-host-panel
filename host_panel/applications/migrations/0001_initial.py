# encoding: utf8
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('containers', '__first__'),
        ('hosts', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='WebApplication',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('short_description', models.CharField(max_length=300, verbose_name=u'Short description')),
                ('name', models.CharField(unique=True, max_length=100, verbose_name=u'Name')),
                ('host', models.ForeignKey(to='hosts.Host', to_field=u'id', verbose_name=u'Host')),
                ('docker_container', models.OneToOneField(null=True, to_field=u'id', blank=True, to='containers.Container', verbose_name=u'Container')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Owner')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
                ('public_app_port', models.PositiveIntegerField(verbose_name=u'Public app port')),
                ('public_ssh_port', models.PositiveIntegerField(null=True, verbose_name=u'Public SSH port', blank=True)),
                ('ide_port', models.PositiveIntegerField(null=True, verbose_name=u'IDE port', blank=True)),
                ('ide_user', models.CharField(max_length=100, verbose_name=u'IDE user', blank=True)),
                ('ide_password', models.CharField(max_length=100, verbose_name=u'IDE password', blank=True)),
            ],
            options={
                u'ordering': ['short_description'],
                u'verbose_name': u'Web application',
                u'verbose_name_plural': u'Web applications',
            },
            bases=(models.Model,),
        ),
    ]
