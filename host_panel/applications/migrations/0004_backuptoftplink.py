# encoding: utf8
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '__first__'),
        ('applications', '0003_backupsettings'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BackupToFTPLink',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('backup_settings', models.ForeignKey(to='applications.BackupSettings', to_field=u'id', verbose_name=u'Backup settings')),
                ('ftpuser', models.ForeignKey(to='hosts.FTPUser', to_field=u'id', verbose_name=u'FTP User')),
                ('ftp_directory', models.CharField(default='backups', max_length=400, verbose_name=u'FTP directory')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Added by')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
            ],
            options={
                u'unique_together': set([('backup_settings', 'ftpuser')]),
                u'verbose_name': u'Backup to FTP links',
                u'verbose_name_plural': u'Backup to FTP links',
            },
            bases=(models.Model,),
        ),
    ]
