# encoding: utf8
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('applications', '0007_postgresqldatabase'),
        ('services', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostgreSQLUser',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=100, verbose_name=u'User name')),
                ('password', models.CharField(max_length=100, verbose_name=u'Password')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Owner')),
                ('webapp', models.ForeignKey(to='applications.WebApplication', to_field=u'id', verbose_name=u'Web application')),
                ('pg_service', models.ForeignKey(to='services.PostgreSQLService', to_field=u'id', verbose_name=u'PG Service')),
            ],
            options={
                u'verbose_name': u'PostgreSQL User',
                u'verbose_name_plural': u'PostgreSQL Users',
            },
            bases=(models.Model,),
        ),
    ]
