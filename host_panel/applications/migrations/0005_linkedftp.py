# encoding: utf8
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '__first__'),
        ('applications', '0004_backuptoftplink'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='LinkedFTP',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('webapp', models.ForeignKey(to='applications.WebApplication', to_field=u'id', verbose_name=u'Web application')),
                ('ftpuser', models.ForeignKey(to='hosts.FTPUser', to_field=u'id', verbose_name=u'FTP User')),
                ('directory', models.CharField(default='webapp/ftp', max_length=400, verbose_name=u'Directory')),
                ('ftp_directory', models.CharField(default='files', max_length=400, verbose_name=u'FTP directory')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Added by')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
            ],
            options={
                u'unique_together': set([('webapp', 'directory')]),
                u'verbose_name': u'Linked FTP',
                u'verbose_name_plural': u'Linked FTPs',
            },
            bases=(models.Model,),
        ),
    ]
