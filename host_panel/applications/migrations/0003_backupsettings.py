# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '__first__'),
        ('applications', '0002_serversettings'),
    ]

    operations = [
        migrations.CreateModel(
            name='BackupSettings',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('webapp', models.OneToOneField(verbose_name=u'Web application', to_field=u'id', to='applications.WebApplication')),
                ('directory', models.CharField(default='/home/webprod/backups', max_length=400, verbose_name=u'Directory')),
                ('cron_time_for_db', models.CharField(default='0 0 * * *', help_text=u'minute (0-59), hour (0-23, 0 = midnight), day (1-31), month (1-12), weekday (0-6, 0 = Sunday)', max_length=200, verbose_name=u'Cron time for DB')),
                ('keep_db_files', models.PositiveIntegerField(default=5, help_text=u'In days', verbose_name=u'Keep DB files')),
                ('db_backup_task', models.ForeignKey(verbose_name=u'DB backup task', to_field=u'id', blank=True, to='hosts.CronTask', null=True)),
                ('cron_time_for_fs', models.CharField(default='0 0 * * *', help_text=u'minute (0-59), hour (0-23, 0 = midnight), day (1-31), month (1-12), weekday (0-6, 0 = Sunday)', max_length=200, verbose_name=u'Cron time for FS')),
                ('keep_fs_files', models.PositiveIntegerField(default=1, help_text=u'In days', verbose_name=u'Keep FS files')),
                ('fs_backup_task', models.ForeignKey(verbose_name=u'FS backup task', to_field=u'id', blank=True, to='hosts.CronTask', null=True)),
            ],
            options={
                u'unique_together': set([('webapp', 'directory')]),
                u'verbose_name': u'Backup settings',
                u'verbose_name_plural': u'Backup settings',
            },
            bases=(models.Model,),
        ),
    ]
