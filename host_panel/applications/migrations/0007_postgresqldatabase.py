# encoding: utf8
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0006_domain'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('services', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='PostgreSQLDatabase',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=u'Name')),
                ('username', models.CharField(max_length=100, verbose_name=u'User name')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Owner')),
                ('webapp', models.ForeignKey(to='applications.WebApplication', to_field=u'id', verbose_name=u'Web application')),
                ('pg_service', models.ForeignKey(to='services.PostgreSQLService', to_field=u'id', verbose_name=u'PG Service')),
            ],
            options={
                u'verbose_name': u'PostgreSQL Database',
                u'verbose_name_plural': u'PostgreSQL Databases',
            },
            bases=(models.Model,),
        ),
    ]
