from django.contrib import admin
from .models import (
    WebApplication,
    PostgreSQLUser,
    PostgreSQLDatabase,
    Domain,
    LinkedFTP,
    BackupSettings,
    ServerSettings,
    BackupToFTPLink,
)


class LinkedFTPInline(admin.TabularInline):
    model = LinkedFTP
    raw_id_fields = ('ftpuser', 'added_by',)
    extra = 0


class PostgreSQLUserInline(admin.TabularInline):
    model = PostgreSQLUser
    raw_id_fields = ('owner',)
    extra = 0


class PostgreSQLDatabaseInline(admin.TabularInline):
    model = PostgreSQLDatabase
    raw_id_fields = ('owner',)
    extra = 0


class BackupToFTPLinkInline(admin.TabularInline):
    model = BackupToFTPLink
    raw_id_fields = ('added_by',)
    extra = 0


class BackupSettingsAdmin(admin.ModelAdmin):
    inlines = [BackupToFTPLinkInline]
    raw_id_fields = ('db_backup_task', 'fs_backup_task')


admin.site.register(BackupSettings, BackupSettingsAdmin)


class BackupSettingsInline(admin.StackedInline):
    model = BackupSettings
    raw_id_fields = ('db_backup_task', 'fs_backup_task')


class ServerSettingsInline(admin.StackedInline):
    model = ServerSettings


class WebApplicationAdmin(admin.ModelAdmin):
    list_display = [
        'name', 'host',
        'owner', 'added',
        'public_app_port', 'public_ssh_port',
    ]
    raw_id_fields = ('owner',)
    inlines = [
        PostgreSQLUserInline,
        PostgreSQLDatabaseInline,
        LinkedFTPInline,
        BackupSettingsInline,
        ServerSettingsInline,
    ]

admin.site.register(WebApplication, WebApplicationAdmin)


class DomainAdmin(admin.ModelAdmin):
    list_display = ['domain', 'protocol', 'added_by', 'added']
    raw_id_fields = ('added_by',)

admin.site.register(Domain, DomainAdmin)
