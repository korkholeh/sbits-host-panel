# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
import json


class WebApplication(models.Model):
    short_description = models.CharField(
        _(u'Short description'), max_length=300)
    name = models.CharField(_(u'Name'), max_length=100, unique=True)
    host = models.ForeignKey(
        'hosts.Host', verbose_name=_(u'Host'),
        related_name='webapps')
    docker_container = models.OneToOneField(
        'containers.Container', verbose_name=_(u'Container'),
        related_name='webapps',
        null=True, blank=True)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_(u'Owner'))
    added = models.DateTimeField(_(u'Added'), auto_now_add=True)
    public_app_port = models.PositiveIntegerField(
        _(u'Public app port'))
    public_ssh_port = models.PositiveIntegerField(
        _(u'Public SSH port'), null=True, blank=True)

    ide_port = models.PositiveIntegerField(
        _(u'IDE port'), null=True, blank=True)
    ide_user = models.CharField(_(u'IDE user'), max_length=100, blank=True)
    ide_password = models.CharField(
        _(u'IDE password'), max_length=100, blank=True)

    class Meta:
        verbose_name = _(u'Web application')
        verbose_name_plural = _(u'Web applications')
        ordering = ['short_description']

    def __unicode__(self):
        return u'{0}@{1}'.format(self.name, self.host.name)

    def get_default_domain(self):
        try:
            return self.domains.get(is_default=True)
        except:
            return None

    def get_extra_domains(self):
        return self.domains.exclude(is_default=True)

    def sync_domains(self):
        from services.utils import sync_domains
        sync_domains(self)

    def remove_domains(self):
        from services.utils import remove_domains
        remove_domains(self)


class PostgreSQLUser(models.Model):
    username = models.CharField(_(u'User name'), max_length=100)
    password = models.CharField(_(u'Password'), max_length=100)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_(u'Owner'),
        related_name='pg_users')
    webapp = models.ForeignKey(
        WebApplication,
        verbose_name=_(u'Web application'),
        related_name='pg_users')
    pg_service = models.ForeignKey(
        'services.PostgreSQLService',
        verbose_name=_(u'PG Service'),
        related_name='pg_users')

    class Meta:
        verbose_name = _(u'PostgreSQL User')
        verbose_name_plural = _(u'PostgreSQL Users')

    def __unicode__(self):
        return u'{0} ({1})'.format(self.username, unicode(self.pg_service))

    def can_be_deleted(self):
        return not PostgreSQLDatabase.objects.filter(username=self.username).exists()

    def add_user_to_server(self):
        from services.utils import postgresql_add_user
        postgresql_add_user(
            host=self.pg_service.host.ip_address,
            port=self.pg_service.public_service_port,
            database='postgres',
            user='postgres',
            password=self.pg_service.postgres_password,
            new_user=self.username,
            new_user_password=self.password,
        )

    def remove_user_from_server(self):
        from services.utils import postgresql_remove_user
        postgresql_remove_user(
            host=self.pg_service.host.ip_address,
            port=self.pg_service.public_service_port,
            database='postgres',
            user='postgres',
            password=self.pg_service.postgres_password,
            username=self.username,
        )


class PostgreSQLDatabase(models.Model):
    name = models.CharField(_(u'Name'), max_length=100)
    username = models.CharField(_(u'User name'), max_length=100)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name=_(u'Owner'),
        related_name='pg_databases')
    webapp = models.ForeignKey(
        WebApplication,
        verbose_name=_(u'Web application'),
        related_name='pg_databases')
    pg_service = models.ForeignKey(
        'services.PostgreSQLService',
        verbose_name=_(u'PG Service'),
        related_name='pg_databases')

    class Meta:
        verbose_name = _(u'PostgreSQL Database')
        verbose_name_plural = _(u'PostgreSQL Databases')

    def __unicode__(self):
        return self.name

    def add_db_to_server(self):
        from services.utils import postgresql_add_database
        postgresql_add_database(
            host=self.pg_service.host.ip_address,
            port=self.pg_service.public_service_port,
            database='postgres',
            user='postgres',
            password=self.pg_service.postgres_password,
            db_name=self.name,
            db_user=self.username)

    def remove_db_from_server(self):
        from services.utils import postgresql_remove_database
        postgresql_remove_database(
            host=self.pg_service.host.ip_address,
            port=self.pg_service.public_service_port,
            database='postgres',
            user='postgres',
            password=self.pg_service.postgres_password,
            db_name=self.name)


class Domain(models.Model):
    PROTOCOLS = (
        ('http', 'HTTP'),
        # ('https', 'HTTPS'),
        # ('tcp', 'TCP'),
    )

    domain = models.CharField(_(u'Domain'), max_length=100, unique=True)
    protocol = models.CharField(
        _(u'Protocol'),
        max_length=20, choices=PROTOCOLS, default='http')
    webapp = models.ForeignKey(
        WebApplication,
        verbose_name=_(u'Applications'),
        related_name='domains')
    hipache = models.ForeignKey(
        'services.Hipache',
        null=True, blank=True,
        related_name='domains')
    is_default = models.BooleanField(
        _(u'Is default'), default=False,
        help_text=_(u'Only one domain for app can be default.'))

    added_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_(u'Added by'))
    added = models.DateTimeField(_(u'Added'), auto_now_add=True)

    class Meta:
        verbose_name = _(u'Domain')
        verbose_name_plural = _(u'Domains')

    def __unicode__(self):
        return self.domain


class LinkedFTP(models.Model):
    webapp = models.ForeignKey(
        WebApplication, verbose_name=_(u'Web application'))
    ftpuser = models.ForeignKey(
        'hosts.FTPUser', verbose_name=_(u'FTP User'))
    directory = models.CharField(
        _(u'Directory'), max_length=400, default='webapp/ftp')
    ftp_directory = models.CharField(
        _(u'FTP directory'), max_length=400, default='files')
    added_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_(u'Added by'))
    added = models.DateTimeField(_(u'Added'), auto_now_add=True)

    class Meta:
        verbose_name = _(u'Linked FTP')
        verbose_name_plural = _(u'Linked FTPs')
        unique_together = ['webapp', 'directory']

    def __unicode__(self):
        return '{0}@{1}:{2}'.format(
            self.ftpuser, self.webapp, self.directory)

    def add_to_server(self):
        from .utils import add_linked_ftp
        add_linked_ftp(self)

    def remove_from_server(self):
        from .utils import remove_linked_ftp
        remove_linked_ftp(self)


class BackupSettings(models.Model):
    webapp = models.OneToOneField(
        WebApplication,
        verbose_name=_(u'Web application'),
        related_name='backup_settings')
    directory = models.CharField(
        _(u'Directory'), max_length=400, default='/home/webprod/backups')
    cron_time_for_db = models.CharField(
        _(u'Cron time for DB'), max_length=200, default='0 0 * * *',
        help_text=_(u'minute (0-59), hour (0-23, 0 = midnight), day (1-31), month (1-12), weekday (0-6, 0 = Sunday)'))
    keep_db_files = models.PositiveIntegerField(
        _(u'Keep DB files'),
        default=5,
        help_text=_(u'In days'))
    db_backup_task = models.ForeignKey(
        'hosts.CronTask', null=True, blank=True,
        verbose_name=_(u'DB backup task'),
        related_name='db_backups')

    cron_time_for_fs = models.CharField(
        _(u'Cron time for FS'), max_length=200, default='0 0 * * *',
        help_text=_(u'minute (0-59), hour (0-23, 0 = midnight), day (1-31), month (1-12), weekday (0-6, 0 = Sunday)'))
    keep_fs_files = models.PositiveIntegerField(
        _(u'Keep FS files'),
        default=1,
        help_text=_(u'In days'))
    fs_backup_task = models.ForeignKey(
        'hosts.CronTask', null=True, blank=True,
        verbose_name=_(u'FS backup task'),
        related_name='fs_backups')

    class Meta:
        verbose_name = _(u'Backup settings')
        verbose_name_plural = _(u'Backup settings')
        unique_together = ['webapp', 'directory']

    def __unicode__(self):
        return unicode(self.webapp)

    def get_db_backup_list(self):
        from .utils import get_file_list
        import re
        import datetime
        import time

        _files = sorted(filter(
            lambda x: 'db_dump' in x,
            get_file_list(self.webapp.host, self.directory),
        ))
        result = []
        _pat = re.compile(r'db_dump-(?P<dbname>\w+)-(?P<date>\d\d\d\d-\d\d-\d\d-\d\d:\d\d)\.gz$')
        for f in _files:
            _data = _pat.search(f).groups()
            t = time.strptime(_data[1], '%Y-%m-%d-%H:%M')
            _date = datetime.datetime(
                year=t.tm_year,
                month=t.tm_mon,
                day=t.tm_mday, hour=t.tm_hour, minute=t.tm_min)
            result.append({
                'dbname': _data[0],
                'date': _date,
                'filename': f,
            })
        return result

    def get_fs_backup_list(self):
        from .utils import get_file_list
        import re
        import datetime
        import time

        _files = sorted(filter(
            lambda x: 'fs_dump' in x,
            get_file_list(self.webapp.host, self.directory),
        ))
        result = []
        _pat = re.compile(r'fs_dump-(?P<date>\d\d\d\d-\d\d-\d\d-\d\d:\d\d)\.tar\.gz$')
        for f in _files:
            _data = _pat.search(f).groups()
            t = time.strptime(_data[0], '%Y-%m-%d-%H:%M')
            _date = datetime.datetime(
                year=t.tm_year,
                month=t.tm_mon,
                day=t.tm_mday, hour=t.tm_hour, minute=t.tm_min)
            result.append({
                'date': _date,
                'filename': f,
            })
        return result

    def sync_backup_settings(self):
        from .utils import sync_backup_settings
        from hosts.models import CronTask
        import os
        _filename = os.path.join(self.directory, 'backup_db.sh')
        _logfile = os.path.join(self.directory, 'backup_db.log')
        _command = '%s >> %s' % (_filename, _logfile)

        sync_backup_settings(self)
        if self.db_backup_task is None:
            db_cron_task = CronTask(
                host=self.webapp.host,
                cron_time=self.cron_time_for_db,
                command=_command,
                added_by=self.webapp.owner)
            db_cron_task.save()
            self.db_backup_task = db_cron_task
            self.save()
        else:
            self.db_backup_task.cron_time = self.cron_time_for_db
            self.db_backup_task.command = _command
            self.db_backup_task.save()

        _filename = os.path.join(self.directory, 'backup_fs.sh')
        _logfile = os.path.join(self.directory, 'backup_fs.log')
        _command = '%s >> %s' % (_filename, _logfile)
        if self.fs_backup_task is None:
            fs_cron_task = CronTask(
                host=self.webapp.host,
                cron_time=self.cron_time_for_fs,
                command=_command,
                added_by=self.webapp.owner)
            fs_cron_task.save()
            self.fs_backup_task = fs_cron_task
            self.save()
        else:
            self.fs_backup_task.cron_time = self.cron_time_for_fs
            self.fs_backup_task.command = _command
            self.fs_backup_task.save()
        self.webapp.host.sync_cron()


class BackupToFTPLink(models.Model):
    backup_settings = models.ForeignKey(
        BackupSettings, verbose_name=_(u'Backup settings'),
        related_name='linked_ftps')
    ftpuser = models.ForeignKey(
        'hosts.FTPUser', verbose_name=_(u'FTP User'))
    ftp_directory = models.CharField(
        _(u'FTP directory'), max_length=400, default='backups')
    added_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_(u'Added by'))
    added = models.DateTimeField(_(u'Added'), auto_now_add=True)

    class Meta:
        verbose_name = _(u'Backup to FTP links')
        verbose_name_plural = _(u'Backup to FTP links')
        unique_together = ['backup_settings', 'ftpuser']

    def __unicode__(self):
        return unicode(self.backup_settings)

    def add_to_server(self):
        from .utils import add_backup_ftp
        add_backup_ftp(self)

    def remove_from_server(self):
        from .utils import remove_backup_ftp
        remove_backup_ftp(self)


class ServerSettings(models.Model):
    PYTHON_VERSIONS = (
        ('/usr/bin/python2.7', '2.7'),
        ('/usr/bin/python3', '3'),
    )
    LOG_LEVELS = (
        (0, _(u'Show only errors and warnings')),
        (1, _(u'Show the most important debugging information')),
        (2, _(u'Show more debugging information')),
        (3, _(u'Show even more debugging information')),
    )

    webapp = models.OneToOneField(
        WebApplication,
        verbose_name=_(u'Web application'),
        related_name='server_settings')
    manual_config = models.BooleanField(
        _(u'Manual config'), default=False)

    public_root = models.CharField(
        _(u'Project public root'),
        max_length=255,
        default='/home/app/webapp/public')
    passenger_app_root = models.CharField(
        _(u'Application root'),
        max_length=255, default='',
        blank=True)
    extra_top = models.TextField(
        _(u'Extra top config'), default='', blank=True)

    set_real_ip_from = models.CharField(
        _(u'Set real IP from'),
        max_length=255,
        blank=True,
        default='172.17.42.1')
    real_ip_header = models.CharField(
        _(u'Real IP header'),
        max_length=255,
        blank=True,
        default='X-Forwarded-For')

    passenger_enabled = models.BooleanField(
        _(u'Passenger enabled'), default=True)
    passenger_user = models.CharField(
        _(u'Application user'), max_length=100,
        default='app')
    passenger_friendly_error_pages = models.BooleanField(
        _(u'Passenger friendly error pages'), default=False)
    passenger_python = models.CharField(
        _(u'Python version'), max_length=100,
        default='/usr/bin/python2.7',
        choices=PYTHON_VERSIONS)

    extra_tail = models.TextField(
        _(u'Extra tail config'), default='', blank=True)

    auth_enabled = models.BooleanField(
        _(u'Auth enabled'), default=False)
    auth_user = models.CharField(
        _(u'Auth user'), max_length=100,
        default='dev')
    auth_password = models.CharField(
        _(u'Auth password'), max_length=100,
        default='dev')
    auth_exlude_ips = models.TextField(
        _(u'Auth exclude IPs'), blank=True, default='',
        help_text=_(u'One IP per line.'))

    passenger_max_pool_size = models.PositiveIntegerField(
        _(u'Max pool size'), null=True, blank=True,
        help_text=_(u'The maximum number of application processes that may simultanously exist.'))
    passenger_min_instances = models.PositiveIntegerField(
        _(u'Min instances'), null=True, blank=True,
        help_text=_(u'This specifies the minimum number of application processes that should exist for a given application.'))
    passenger_pool_idle_time = models.PositiveIntegerField(
        _(u'Pool idle time'), null=True, blank=True,
        help_text=_(u'The maximum number of seconds that an application process may be idle.'))
    passenger_max_preloader_idle_time = models.PositiveIntegerField(
        _(u'Max preloader idle time'), null=True, blank=True,
        help_text=_(u'The preloader process has an idle timeout, just like the backend processes spawned by Phusion Passenger do.'))
    passenger_start_timeout = models.PositiveIntegerField(
        _(u'Start timeout'), null=True, blank=True,
        help_text=_(u'Specifies a timeout for the startup of application processes.'))
    passenger_max_requests = models.PositiveIntegerField(
        _(u'Max requests'), null=True, blank=True,
        help_text=_(u'The maximum number of requests an application process will process.'))

    passenger_pre_start = models.BooleanField(
        _(u'Passenger pre start'), default=False)  # Use main domain for ping

    passenger_max_request_queue_size = models.PositiveIntegerField(
        _(u'Max request queue size'), null=True, blank=True,
        help_text=_(u'If the queue is already at this specified limit, then Phusion Passenger will immediately send a "503 Service Unavailable" error to any incoming requests.'))
    passenger_request_queue_overflow_status_code = models.PositiveIntegerField(
        _(u'Request queue overflow status code'), null=True, blank=True,
        help_text=_(u'This option allows you to customize the HTTP status code that is sent back when the request queue is full.'))

    passenger_ignore_client_abort = models.BooleanField(
        _(u'Ignore client abort'), default=False)
    passenger_intercept_errors = models.BooleanField(
        _(u'Passenger intercept errors'), default=False)
    passenger_pass_header = models.CharField(
        _(u'Passenger pass header'), max_length=255,
        blank=True,
        default='',
        help_text=_(u'Some headers generated by backend applications are not forwarded to the HTTP client, e.g. X-Accel-Redirect which is directly processed by Nginx and then discarded from the final response.'))
    passenger_buffer_response = models.BooleanField(
        _(u'Buffer response'), default=False)  # TODO: need extra options

    passenger_log_level = models.PositiveIntegerField(
        _(u'Log level'), null=True, blank=True,
        choices=LOG_LEVELS)
    passenger_debug_log_file = models.CharField(
        _(u'Debug log file'), max_length=255,
        blank=True,
        default='',
        help_text=_(u'By default Phusion Passenger debugging and error messages are written to the global web server error log. This option allows one to specify the file that debugging and error messages should be written to instead.'))

    class Meta:
        verbose_name = _(u'Server settings')
        verbose_name_plural = _(u'Server settings')

    def __unicode__(self):
        return unicode(self.webapp)

    def get_config_file(self):
        from .utils import get_server_config
        return get_server_config(self.webapp)

    def save_config_file(self, config, auth_user=None, auth_pwd=None):
        from .utils import save_config_file
        save_config_file(self.webapp, config, auth_user, auth_pwd)

    def get_exclude_ips(self):
        result = []
        for i in self.auth_exlude_ips.splitlines():
            s = i.strip()
            if s:
                result.append(s)
        return result

    def compile_config(self):
        from django.template.loader import render_to_string
        result = render_to_string(
            'configs/webapp.conf',
            {
                'server_settings': self,
                'main_domain': self.webapp.get_default_domain(),
            })
        if self.auth_enabled:
            self.save_config_file(
                result,
                auth_user=self.auth_user,
                auth_pwd=self.auth_password)
        else:
            self.save_config_file(result)
        return result
