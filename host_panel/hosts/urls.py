# coding: utf-8
from django.conf.urls import patterns, url
from .views import (
    HostListView,
    HostDetailView,
    HostCreateView,
    HostSSHKeyListView,
    HostSSHKeyCreateView,
    HostSSHKeyDeleteView,
    FTPUserListView,
    FTPUserCreateView,
    FTPUserDeleteView,
    DatabaseServiceListView,
    DatabaseServiceCreateView,
    DatabaseServiceDetailView,
    DatabaseServiceDeleteView,
    DatabaseServiceSSHKeyCreateView,
    DatabaseServiceSSHKeyDeleteView,
    HipacheInfoView,
    HipacheSSHKeyCreateView,
    HipacheSSHKeyDeleteView,
    CronTaskListView,
    CronTaskCreateView,
    CronTaskUpdateView,
    CronTaskDeleteView,
)

urlpatterns = patterns(
    'hosts.views',
    url(
        r'^(?P<host_pk>\d+)/routing/sshkeys/(?P<pk>\d+)/remove/$',
        HipacheSSHKeyDeleteView.as_view(), name='host_routing_remove_sshkey'),
    url(
        r'^(?P<host_pk>\d+)/routing/sshkeys/add/$',
        HipacheSSHKeyCreateView.as_view(), name='host_routing_add_sshkey'),
    url(
        r'^(?P<host_pk>\d+)/routing/install/$',
        'install_hipache', name='host_install_routing'),
    url(
        r'^(?P<host_pk>\d+)/routing/update-hipache-db/$',
        'update_hipache_db', name='host_update_hipache_db'),
    url(
        r'^(?P<host_pk>\d+)/routing/$',
        HipacheInfoView.as_view(), name='host_routing'),

    url(
        r'^(?P<host_pk>\d+)/database-services/(?P<db_pk>\d+)/sshkeys/(?P<pk>\d+)/remove/$',
        DatabaseServiceSSHKeyDeleteView.as_view(),
        name='host_database_service_remove_sshkey'),
    url(
        r'^(?P<host_pk>\d+)/database-services/(?P<pk>\d+)/sshkeys/add/$',
        DatabaseServiceSSHKeyCreateView.as_view(),
        name='host_database_service_add_sshkey'),
    url(
        r'^(?P<host_pk>\d+)/database-services/(?P<pk>\d+)/remove/$',
        DatabaseServiceDeleteView.as_view(),
        name='host_remove_database_service'),
    url(
        r'^(?P<host_pk>\d+)/database-services/(?P<pk>\d+)/$',
        DatabaseServiceDetailView.as_view(), name='host_database_service'),
    url(
        r'^(?P<host_pk>\d+)/database-services/add/$',
        DatabaseServiceCreateView.as_view(), name='host_add_database_service'),
    url(
        r'^(?P<host_pk>\d+)/database-services/$',
        DatabaseServiceListView.as_view(), name='host_database_services'),

    url(
        r'^(?P<host_pk>\d+)/ftpusers/add/$',
        FTPUserCreateView.as_view(), name='host_add_ftpuser'),
    url(
        r'^(?P<host_pk>\d+)/ftpusers/(?P<pk>\d+)/remove/$',
        FTPUserDeleteView.as_view(), name='host_remove_ftpuser'),
    url(
        r'^(?P<host_pk>\d+)/ftpusers/$',
        FTPUserListView.as_view(), name='host_ftpusers'),

    url(
        r'^(?P<host_pk>\d+)/run-command/(?P<button_id>\d+)/$',
        'host_run_command', name='run_command'),

    url(
        r'^(?P<host_pk>\d+)/sshkeys/add/$',
        HostSSHKeyCreateView.as_view(), name='host_add_sshkey'),
    url(
        r'^(?P<host_pk>\d+)/sshkeys/(?P<pk>\d+)/remove/$',
        HostSSHKeyDeleteView.as_view(), name='host_remove_sshkey'),
    url(
        r'^(?P<host_pk>\d+)/sshkeys/$',
        HostSSHKeyListView.as_view(), name='host_sshkeys'),

    url(
        r'^(?P<host_pk>\d+)/crontasks/add/$',
        CronTaskCreateView.as_view(), name='host_add_crontask'),
    url(
        r'^(?P<host_pk>\d+)/crontasks/(?P<pk>\d+)/edit/$',
        CronTaskUpdateView.as_view(), name='host_edit_crontask'),
    url(
        r'^(?P<host_pk>\d+)/crontasks/(?P<pk>\d+)/remove/$',
        CronTaskDeleteView.as_view(), name='host_remove_crontask'),
    url(
        r'^(?P<host_pk>\d+)/crontasks/$',
        CronTaskListView.as_view(), name='host_crontasks'),

    url(
        r'^(?P<pk>\d+)/host-setup.sh$',
        'setup_script', name='host_setup_script'),
    url(
        r'^(?P<pk>\d+)/mark-manageable/$',
        'mark_manageable', name='host_mark_manageable'),

    url(
        r'^(?P<pk>\d+)/detail/$',
        HostDetailView.as_view(), name='host_detail'),
    url(r'^add/$', HostCreateView.as_view(), name='add_host'),
    url(r'^$', HostListView.as_view(), name='host_list'),
)
