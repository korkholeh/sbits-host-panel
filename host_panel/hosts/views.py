# coding: utf-8
from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import (
    HttpResponseForbidden,
    HttpResponseRedirect,
    HttpResponse,
)
from django.shortcuts import get_object_or_404
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView
from django.views.generic import CreateView, UpdateView, DeleteView
from django.views.generic import TemplateView
from django.template import RequestContext
from django.conf import settings

from services.models import (
    PostgreSQLService,
)
from services.forms import (
    AddPostgreSQLServer,
)
from containers.forms import (
    AddContainerSSHKeyForm,
)
from containers.models import (
    ContainerSSHKey,
)
from .models import (
    Host,
    HostSSHKey,
    HostAdminButton,
    FTPUser,
    CronTask,
)
from .forms import (
    AddSSHKeyForm,
    AddFTPUserForm,
    EditHostForm,
    AddCronTaskForm,
)


class HostListView(ListView):
    model = Host
    context_object_name = 'hosts'


class HostDetailView(DetailView):
    model = Host
    context_object_name = 'host'

    def get_context_data(self, **kwargs):
        from containers.models import DockerImage
        context = super(
            HostDetailView, self).get_context_data(**kwargs)
        context.update({
            'hide_sidebar': not self.get_object().is_manageable,
            'manage_key': settings.MAINTAINABLE_SSH_KEY,
            'panel_domain': settings.HOST_PANEL_DOMAIN,
            'docker_images': DockerImage.objects.all(),
        })
        return context


def setup_script(request, pk):
    host = get_object_or_404(Host, pk=pk)
    response = HttpResponse(
        host.server_type.build_script,
        content_type='text/plain')
    return response


def mark_manageable(request, pk):
    host = get_object_or_404(Host, pk=pk)
    host.is_manageable = True
    host.save()
    return redirect(reverse('hosts:host_detail', args=(host.pk,)))


class HostCreateView(CreateView):
    model = Host
    form_class = EditHostForm

    def get_success_url(self):
        return reverse('hosts:host_list')


class HostSSHKeyListView(ListView):
    model = HostSSHKey
    context_object_name = 'keys'

    def get_queryset(self):
        host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        return HostSSHKey.objects.filter(
            host=host,
            key__owner=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(
            HostSSHKeyListView, self).get_context_data(**kwargs)
        context.update({
            'host': Host.objects.get(pk=self.kwargs.get('host_pk'))
        })
        return context


class HostSSHKeyCreateView(CreateView):
    model = HostSSHKey
    form_class = AddSSHKeyForm

    def get_form_kwargs(self):
        kwargs = super(HostSSHKeyCreateView, self).get_form_kwargs()
        self.host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        kwargs['customer'] = self.request.user
        kwargs['host'] = self.host
        return kwargs

    def form_valid(self, form):
        from .utils import add_sshkey_to_host
        sshkey = form.save(commit=False)
        sshkey.added_by = self.request.user
        sshkey.host = self.host
        # Connect to host and add public key
        add_sshkey_to_host(self.host, sshkey)

        sshkey.save()
        return super(HostSSHKeyCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(
            HostSSHKeyCreateView, self).get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('hosts:host_sshkeys', args=(self.host.pk,))


class HostSSHKeyDeleteView(DeleteView):
    model = HostSSHKey

    def get_object(self):
        host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        self.host = host
        result = get_object_or_404(
            HostSSHKey,
            host=host,
            pk=self.kwargs.get('pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super(
            HostSSHKeyDeleteView, self).get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        from .utils import remove_sshkey_from_host
        self.object = self.get_object()
        remove_sshkey_from_host(self.host, self.get_object())
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('hosts:host_sshkeys', args=(self.host.pk,))


class FTPUserListView(ListView):
    model = FTPUser
    context_object_name = 'ftpusers'

    def get_queryset(self):
        host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        return FTPUser.objects.filter(host=host)

    def get_context_data(self, **kwargs):
        context = super(
            FTPUserListView, self).get_context_data(**kwargs)
        context.update({
            'host': Host.objects.get(pk=self.kwargs.get('host_pk'))
        })
        return context


class FTPUserCreateView(CreateView):
    model = FTPUser
    form_class = AddFTPUserForm

    def get_form_kwargs(self):
        kwargs = super(FTPUserCreateView, self).get_form_kwargs()
        self.host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        kwargs['host'] = self.host
        return kwargs

    def form_valid(self, form):
        from .utils import add_ftp_user
        ftpuser = form.save(commit=False)
        ftpuser.host = self.host
        ftpuser.owner = self.request.user
        add_ftp_user(ftpuser)
        ftpuser.save()
        return super(FTPUserCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(
            FTPUserCreateView, self).get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('hosts:host_ftpusers', args=(self.host.pk,))


class FTPUserDeleteView(DeleteView):
    model = FTPUser

    def get_object(self):
        host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        self.host = host
        result = get_object_or_404(
            FTPUser,
            host=host,
            pk=self.kwargs.get('pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super(
            FTPUserDeleteView, self).get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        from .utils import remove_ftp_user
        self.object = self.get_object()
        remove_ftp_user(self.object)
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('hosts:host_ftpusers', args=(self.host.pk,))


def host_run_command(request, host_id, button_id):
    from .utils import run_commands_on_host
    host = get_object_or_404(Host, pk=host_id)

    button = get_object_or_404(HostAdminButton, pk=button_id)
    result = run_commands_on_host(
        host.ip_address,
        host.main_user,
        host.sudo_password,
        button.commands,
        button.run_with_sudo)

    return render_to_response(
        'hosts/command_result.html',
        RequestContext(request, {
            'host': host,
            'button': button,
            'result': result,
        }))


class DatabaseServiceListView(ListView):
    model = PostgreSQLService
    template_name = 'hosts/postgresqlservice_list.html'
    context_object_name = 'db_servers'

    def get_queryset(self):
        host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        return PostgreSQLService.objects.filter(host=host).order_by('name')

    def get_context_data(self, **kwargs):
        context = super(
            DatabaseServiceListView, self).get_context_data(**kwargs)
        context.update({
            'host': Host.objects.get(pk=self.kwargs.get('host_pk'))
        })
        return context


class DatabaseServiceCreateView(CreateView):
    model = PostgreSQLService
    form_class = AddPostgreSQLServer
    template_name = 'hosts/postgresqlservice_form.html'

    def get_form_kwargs(self):
        kwargs = super(DatabaseServiceCreateView, self).get_form_kwargs()
        self.host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        kwargs['host'] = self.host
        return kwargs

    def form_valid(self, form):
        from containers.models import Container
        pg_service = form.save(commit=False)
        pg_service.host = self.host
        docker_image = form.cleaned_data['docker_image']

        _container = Container(
            name=pg_service.name,
            host=self.host,
            docker_image=docker_image)
        _container.save()
        _container.run(env_variables={
            'PG_PASSWORD': pg_service.postgres_password,
        })

        pg_service.docker_container = _container
        pg_service.public_service_port = \
            _container.get_port(name='postgres')
        pg_service.public_ssh_port = \
            _container.get_port(name='ssh')
        pg_service.save()
        return super(DatabaseServiceCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(
            DatabaseServiceCreateView, self).get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('hosts:host_database_services', args=(self.host.pk,))


class DatabaseServiceDetailView(DetailView):
    model = PostgreSQLService
    context_object_name = 'db_service'
    template_name = 'hosts/postgresqlservice_detail.html'

    def get_object(self):
        host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        self.host = host
        result = get_object_or_404(
            PostgreSQLService,
            host=host,
            pk=self.kwargs.get('pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super(
            DatabaseServiceDetailView, self).get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context


class DatabaseServiceDeleteView(DeleteView):
    model = PostgreSQLService
    context_object_name = 'db_service'
    template_name = 'hosts/postgresql_confirm_delete.html'

    def get_object(self):
        host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        self.host = host
        result = get_object_or_404(
            PostgreSQLService,
            host=host,
            pk=self.kwargs.get('pk'))
        self.db_service = result
        return result

    def get_context_data(self, **kwargs):
        context = super(
            DatabaseServiceDeleteView, self).get_context_data(**kwargs)
        context.update({
            'host': self.host,
            'db_service': self.db_service,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.object.docker_container.destroy()
        self.object.delete()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('hosts:host_database_services', args=(self.host.pk,))


class DatabaseServiceSSHKeyCreateView(CreateView):
    model = ContainerSSHKey
    form_class = AddContainerSSHKeyForm
    template_name = 'hosts/postgresql_add_sshkey.html'

    def get_form_kwargs(self):
        kwargs = super(DatabaseServiceSSHKeyCreateView, self).get_form_kwargs()
        self.host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        self.db_service = get_object_or_404(
            PostgreSQLService,
            host=self.host,
            pk=self.kwargs.get('pk'))
        kwargs['container'] = self.db_service.docker_container
        kwargs['customer'] = self.request.user
        return kwargs

    def form_valid(self, form):
        sshkey = form.save(commit=False)
        sshkey.docker_container = self.db_service.docker_container
        sshkey.added_by = self.request.user
        sshkey.save()
        self.db_service.docker_container.sync_sshkeys()
        return super(DatabaseServiceSSHKeyCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(
            DatabaseServiceSSHKeyCreateView, self).get_context_data(**kwargs)
        context.update({
            'host': self.host,
            'db_service': self.db_service,
        })
        return context

    def get_success_url(self):
        return reverse(
            'hosts:host_database_service',
            args=(self.host.pk, self.db_service.pk))


class DatabaseServiceSSHKeyDeleteView(DeleteView):
    model = ContainerSSHKey
    template_name = 'hosts/postgresql_sshkey_confirm_delete.html'

    def get_object(self):
        self.host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        self.db_service = get_object_or_404(
            PostgreSQLService,
            host=self.host,
            pk=self.kwargs.get('db_pk'))
        result = get_object_or_404(
            ContainerSSHKey,
            docker_container=self.db_service.docker_container,
            pk=self.kwargs.get('pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super(
            DatabaseServiceSSHKeyDeleteView, self).get_context_data(**kwargs)
        context.update({
            'host': self.host,
            'db_service': self.db_service,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.object.delete()
        self.db_service.docker_container.sync_sshkeys()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse(
            'hosts:host_database_service',
            args=(self.host.pk, self.db_service.pk))


class HipacheInfoView(TemplateView):
    template_name = 'hosts/hipache.html'

    def get_context_data(self, **kwargs):
        host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        context = super(
            HipacheInfoView, self).get_context_data(**kwargs)
        context.update({
            'host': host,
        })
        return context


def install_hipache(request, host_pk):
    from containers.models import Container, DockerImage
    from services.models import Hipache
    host = get_object_or_404(Host, pk=host_pk)
    _image = get_object_or_404(
        DockerImage, image_name=settings.HIPACHI_DOCKER_IMAGE)
    _container = Container(
        name='hipache',
        host=host,
        docker_image=_image)
    _container.save()
    _container.run()

    hipache = Hipache(
        host=host,
        docker_container=_container,
        public_service_port=_container.get_port(name='webservice'),
        public_ssh_port=_container.get_port(name='ssh'))
    hipache.save()
    return redirect(reverse('hosts:host_routing', args=[host.pk]))


def update_hipache_db(request, host_pk):
    host = get_object_or_404(Host, pk=host_pk)
    host.hipache.update_hipache_db()
    return redirect(reverse('hosts:host_routing', args=[host.pk]))


class HipacheSSHKeyCreateView(CreateView):
    model = ContainerSSHKey
    form_class = AddContainerSSHKeyForm
    template_name = 'hosts/hipache_add_sshkey.html'

    def get_form_kwargs(self):
        kwargs = super(HipacheSSHKeyCreateView, self).get_form_kwargs()
        self.host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        kwargs['container'] = self.host.hipache.docker_container
        kwargs['customer'] = self.request.user
        return kwargs

    def form_valid(self, form):
        sshkey = form.save(commit=False)
        sshkey.docker_container = self.host.hipache.docker_container
        sshkey.added_by = self.request.user
        sshkey.save()
        self.host.hipache.docker_container.sync_sshkeys()
        return super(HipacheSSHKeyCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(
            HipacheSSHKeyCreateView, self).get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('hosts:host_routing', args=(self.host.pk,))


class HipacheSSHKeyDeleteView(DeleteView):
    model = ContainerSSHKey
    template_name = 'hosts/hipache_sshkey_confirm_delete.html'

    def get_object(self):
        host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        self.host = host
        result = get_object_or_404(
            ContainerSSHKey,
            docker_container=host.hipache.docker_container,
            pk=self.kwargs.get('pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super(
            HipacheSSHKeyDeleteView, self).get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.object.delete()
        self.host.hipache.docker_container.sync_sshkeys()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('hosts:host_routing', args=(self.host.pk,))


class CronTaskListView(ListView):
    model = CronTask
    context_object_name = 'cron_tasks'

    def get_queryset(self):
        self.host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        return CronTask.objects.filter(host=self.host)

    def get_context_data(self, **kwargs):
        context = super(
            CronTaskListView, self).get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context


class CronTaskCreateView(CreateView):
    model = CronTask
    form_class = AddCronTaskForm

    def dispatch(self, request, *args, **kwargs):
        self.host = get_object_or_404(Host, pk=kwargs.get('host_pk'))
        return super(
            CronTaskCreateView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        cron_task = form.save(commit=False)
        cron_task.added_by = self.request.user
        cron_task.host = self.host
        cron_task.save()
        cron_task.host.sync_cron()
        return super(CronTaskCreateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(
            CronTaskCreateView, self).get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('hosts:host_crontasks', args=(self.host.pk,))


class CronTaskUpdateView(UpdateView):
    model = CronTask
    form_class = AddCronTaskForm

    def dispatch(self, request, *args, **kwargs):
        self.host = get_object_or_404(Host, pk=kwargs.get('host_pk'))
        return super(
            CronTaskUpdateView, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        cron_task = form.save()
        cron_task.host.sync_cron()
        return super(CronTaskUpdateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(
            CronTaskUpdateView, self).get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def get_success_url(self):
        return reverse('hosts:host_crontasks', args=(self.host.pk,))


class CronTaskDeleteView(DeleteView):
    model = CronTask

    def get_object(self):
        host = get_object_or_404(Host, pk=self.kwargs.get('host_pk'))
        self.host = host
        result = get_object_or_404(
            CronTask,
            host=host,
            pk=self.kwargs.get('pk'))
        return result

    def get_context_data(self, **kwargs):
        context = super(
            CronTaskDeleteView, self).get_context_data(**kwargs)
        context.update({
            'host': self.host,
        })
        return context

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.object.delete()
        self.host.sync_cron()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse('hosts:host_crontasks', args=(self.host.pk,))
