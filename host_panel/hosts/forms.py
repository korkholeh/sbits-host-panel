# coding: utf-8
from django import forms
from django.forms import widgets
from django.utils.translation import ugettext_lazy as _
from .models import Host, HostSSHKey, FTPUser, CronTask, ServerType
from codemirror import CodeMirrorTextarea


class ServerTypeForm(forms.ModelForm):
    class Meta:
        model = ServerType
        fields = '__all__'
        widgets = {
            'build_script': CodeMirrorTextarea(
                mode="shell", config={'fixedGutter': True}),
        }


class EditHostForm(forms.ModelForm):
    class Meta:
        model = Host
        fields = '__all__'
        widgets = {
            'sudo_password': forms.PasswordInput(),
        }


class AddSSHKeyForm(forms.ModelForm):
    class Meta:
        model = HostSSHKey
        fields = ['key']

    def __init__(self, customer, host, *args, **kwargs):
        from sshkeys.models import SSHKey
        super(AddSSHKeyForm, self).__init__(*args, **kwargs)
        self.host = host
        self.fields['key'].queryset = SSHKey.objects.filter(owner=customer)

    def clean_key(self):
        from .models import HostSSHKey
        already_exist = HostSSHKey.objects.filter(
            host=self.host, key=self.cleaned_data['key']).exists()

        if not already_exist:
            return self.cleaned_data['key']
        raise forms.ValidationError(_(u'This key already added to host.'))


class AddFTPUserForm(forms.ModelForm):
    class Meta:
        model = FTPUser
        fields = ['username', 'password']
        widgets = {
            'password': forms.PasswordInput(),
        }

    def __init__(self, host, *args, **kwargs):
        super(AddFTPUserForm, self).__init__(*args, **kwargs)
        self.host = host

    def clean_username(self):
        if not self.cleaned_data['username'].startswith('ftp-'):
            raise forms.ValidationError(_(u'Name of ftp user must starts with "ftp-".'))
        already_exist = FTPUser.objects.filter(
            host=self.host,
            username__exact=self.cleaned_data['username']).exists()

        if not already_exist:
            return self.cleaned_data['username']
        raise forms.ValidationError(_(u'User already exists.'))


class AddCronTaskForm(forms.ModelForm):
    class Meta:
        model = CronTask
        fields = ['active', 'cron_time', 'command']
        widgets = {
            'command': widgets.TextInput(),
        }
