# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class ServerType(models.Model):
    name = models.CharField(_(u'Name'), max_length=100)
    build_script = models.TextField(_(u'Build script'))

    class Meta:
        verbose_name = _(u'Server type')
        verbose_name_plural = _(u'Server types')

    def __unicode__(self):
        return self.name


class Host(models.Model):
    """This is a physical host in 7bits infrastructure.
    """
    name = models.CharField(_(u'Name'), max_length=100)
    server_type = models.ForeignKey(ServerType, verbose_name=_(u'Server type'))
    ip_address = models.IPAddressField(_(u'IP Address'))
    host_domain = models.CharField(
        _(u'Host domain'), max_length=200, default='my-host.com')
    description = models.TextField(
        _(u'Description'), blank=True,
        help_text=_(u'You can use Markdown.'))
    main_user = models.CharField(_(u'Main user'), max_length=50)
    is_manageable = models.BooleanField(
        _(u'Is manageable'), default=False)
    sudo_password = models.CharField(_(u'Sudo password'), max_length=50)

    class Meta:
        verbose_name = _(u'Host')
        verbose_name_plural = _(u'Hosts')
        ordering = ['name']

    def __unicode__(self):
        return self.name

    def get_buttons(self):
        return self.buttons.all()

    def get_ssh_command(self):
        return 'ssh {user}@{ip}'.format(
            user=self.main_user,
            ip=self.ip_address,
        )

    def get_host_domain(self):
        return self.host_domain

    def get_description_as_html(self):
        import markdown
        md = markdown.Markdown(
            safe_mode='replace',
            html_replacement_text='--RAW HTML NOT ALLOWED--')
        return md.convert(self.description)

    def sync_containers(self):
        from containers.models import DockerImage
        from containers.utils import get_container_ids, get_container_info
        from django.core.exceptions import ObjectDoesNotExist
        from django.utils.timezone import utc
        import datetime
        import json
        _ids = get_container_ids(self)
        # print _ids
        for i in _ids:
            id = i[0]
            is_running = i[1]
            info = get_container_info(self, id)[0]
            image_name = info['Config']['Image']
            try:
                docker_image = DockerImage.objects.get(image_name=image_name)
            except DockerImage.DoesNotExist:
                docker_image = None

            try:
                _container = self.containers.get(container_id=id)
                _container.docker_image = docker_image
                _container.name = info['Name'][1:]
                _container.meta = json.dumps(info)
                _container.is_running = is_running
                _container.last_sync = \
                    datetime.datetime.utcnow().replace(tzinfo=utc)
                _container.save()
            except ObjectDoesNotExist:
                _container = self.containers.create(
                    name=info['Name'][1:],
                    docker_image=docker_image,
                    host=self,
                    container_id=info['ID'],
                    meta=json.dumps(info),
                    is_running=is_running,
                    last_sync=datetime.datetime.utcnow().replace(tzinfo=utc),
                )
        _available_ids = [i[0] for i in _ids]
        containers_for_delete = self.containers.exclude(
            container_id__in=_available_ids)
        if containers_for_delete.count() > 0:
            containers_for_delete.delete()

    def has_router(self):
        if self.hipache:
            return True
        else:
            return False

    def sync_cron(self):
        from .utils import sync_cron_tasks
        sync_cron_tasks(self)


class HostSSHKey(models.Model):
    host = models.ForeignKey(Host, verbose_name=_(u'Host'))
    key = models.ForeignKey('sshkeys.SSHKey', verbose_name=_(u'Key'))
    added_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_(u'Added by'))
    added = models.DateTimeField(_(u'Added'), auto_now_add=True)

    class Meta:
        verbose_name = _(u'Host SSH key')
        verbose_name_plural = _(u'Host SSH keys')

    def __unicode__(self):
        return self.key.name


class HostAdminButton(models.Model):
    hosts = models.ManyToManyField(
        Host, verbose_name=_(u'Hosts'), related_name='buttons')
    button_label = models.CharField(_(u'Button label'), max_length=100)
    button_class = models.CharField(_(u'Button label'), max_length=100)
    tooltip = models.TextField(_(u'Tooltip'), blank=True)
    commands = models.TextField(_(u'Commands'), blank=True)
    run_with_sudo = models.BooleanField(_(u'Run with sudo'), default=False)
    new_group = models.BooleanField(_(u'New group'), default=False)
    position = models.IntegerField(_(u'Position'), default=1)

    class Meta:
        verbose_name = _(u'Host admin button')
        verbose_name_plural = _(u'Host admin buttons')
        ordering = ['position']

    def __unicode__(self):
        return self.button_label


class FTPUser(models.Model):
    username = models.CharField(_(u'User name'), max_length=100)
    password = models.CharField(_(u'Password'), max_length=50)
    host = models.ForeignKey(Host, verbose_name=_(u'Host'))
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_(u'Owner'))
    added = models.DateTimeField(_(u'Added'), auto_now_add=True)

    class Meta:
        verbose_name = _(u'FTP User')
        verbose_name_plural = _(u'FTP Users')
        unique_together = ['username', 'host']

    def __unicode__(self):
        return '{0}@{1}'.format(self.username, self.host.name)

    def can_remove(self):
        # TODO: need to implement
        return True


class CronTask(models.Model):
    host = models.ForeignKey(
        Host, verbose_name=_(u'Host'),
        related_name='cron_tasks')
    active = models.BooleanField(_(u'Active'), default=True)
    cron_time = models.CharField(
        _(u'Cron time'), max_length=200, default='0 0 * * *',
        help_text=_(u'minute (0-59), hour (0-23, 0 = midnight), day (1-31), month (1-12), weekday (0-6, 0 = Sunday)'))
    command = models.TextField(_(u'Command'), default='echo "Hello"')
    added = models.DateTimeField(_(u'Added'), auto_now_add=True)
    modified = models.DateTimeField(_(u'Modified'), auto_now=True)
    added_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_(u'Added by'))

    class Meta:
        verbose_name = _(u'Cron task')
        verbose_name_plural = _(u'Cron tasks')
        ordering = ['command']

    def __unicode__(self):
        return u'%s %s' % (self.cron_time, self.command)

    def is_backup_task(self):
        return hasattr(self, 'db_backups') or hasattr(self, 'fs_backups')
