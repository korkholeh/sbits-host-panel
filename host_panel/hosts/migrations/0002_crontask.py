# encoding: utf8
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CronTask',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('host', models.ForeignKey(to='hosts.Host', to_field=u'id', verbose_name=u'Host')),
                ('active', models.BooleanField(default=True, verbose_name=u'Active')),
                ('cron_time', models.CharField(default='0 0 * * *', help_text=u'minute (0-59), hour (0-23, 0 = midnight), day (1-31), month (1-12), weekday (0-6, 0 = Sunday)', max_length=200, verbose_name=u'Cron time')),
                ('command', models.TextField(default='echo "Hello"', verbose_name=u'Command')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name=u'Modified')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Added by')),
            ],
            options={
                u'ordering': ['command'],
                u'verbose_name': u'Cron task',
                u'verbose_name_plural': u'Cron tasks',
            },
            bases=(models.Model,),
        ),
    ]
