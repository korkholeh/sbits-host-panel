# encoding: utf8
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('hosts', '0002_crontask'),
    ]

    operations = [
        migrations.CreateModel(
            name='FTPUser',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=100, verbose_name=u'User name')),
                ('password', models.CharField(max_length=50, verbose_name=u'Password')),
                ('host', models.ForeignKey(to='hosts.Host', to_field=u'id', verbose_name=u'Host')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Owner')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
            ],
            options={
                u'unique_together': set([('username', 'host')]),
                u'verbose_name': u'FTP User',
                u'verbose_name_plural': u'FTP Users',
            },
            bases=(models.Model,),
        ),
    ]
