# encoding: utf8
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    replaces = [('hosts', '0001_initial'), ('hosts', '0002_crontask'), ('hosts', '0003_ftpuser'), ('hosts', '0004_hostadminbutton'), ('hosts', '0005_hostsshkey')]

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('sshkeys', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='ServerType',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=u'Name')),
                ('build_script', models.TextField(verbose_name=u'Build script')),
            ],
            options={
                u'verbose_name': u'Server type',
                u'verbose_name_plural': u'Server types',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Host',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=u'Name')),
                ('server_type', models.ForeignKey(to='hosts.ServerType', to_field=u'id', verbose_name=u'Server type')),
                ('ip_address', models.IPAddressField(verbose_name=u'IP Address')),
                ('host_domain', models.CharField(default='my-host.com', max_length=200, verbose_name=u'Host domain')),
                ('description', models.TextField(help_text=u'You can use Markdown.', verbose_name=u'Description', blank=True)),
                ('main_user', models.CharField(max_length=50, verbose_name=u'Main user')),
                ('is_manageable', models.BooleanField(default=False, verbose_name=u'Is manageable')),
                ('sudo_password', models.CharField(max_length=50, verbose_name=u'Sudo password')),
            ],
            options={
                u'ordering': ['name'],
                u'verbose_name': u'Host',
                u'verbose_name_plural': u'Hosts',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CronTask',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('host', models.ForeignKey(to='hosts.Host', to_field=u'id', verbose_name=u'Host')),
                ('active', models.BooleanField(default=True, verbose_name=u'Active')),
                ('cron_time', models.CharField(default='0 0 * * *', help_text=u'minute (0-59), hour (0-23, 0 = midnight), day (1-31), month (1-12), weekday (0-6, 0 = Sunday)', max_length=200, verbose_name=u'Cron time')),
                ('command', models.TextField(default='echo "Hello"', verbose_name=u'Command')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name=u'Modified')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Added by')),
            ],
            options={
                u'ordering': ['command'],
                u'verbose_name': u'Cron task',
                u'verbose_name_plural': u'Cron tasks',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FTPUser',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(max_length=100, verbose_name=u'User name')),
                ('password', models.CharField(max_length=50, verbose_name=u'Password')),
                ('host', models.ForeignKey(to='hosts.Host', to_field=u'id', verbose_name=u'Host')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Owner')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
            ],
            options={
                u'unique_together': set([('username', 'host')]),
                u'verbose_name': u'FTP User',
                u'verbose_name_plural': u'FTP Users',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HostAdminButton',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('button_label', models.CharField(max_length=100, verbose_name=u'Button label')),
                ('button_class', models.CharField(max_length=100, verbose_name=u'Button label')),
                ('tooltip', models.TextField(verbose_name=u'Tooltip', blank=True)),
                ('commands', models.TextField(verbose_name=u'Commands', blank=True)),
                ('run_with_sudo', models.BooleanField(default=False, verbose_name=u'Run with sudo')),
                ('new_group', models.BooleanField(default=False, verbose_name=u'New group')),
                ('position', models.IntegerField(default=1, verbose_name=u'Position')),
                ('hosts', models.ManyToManyField(to='hosts.Host', verbose_name=u'Hosts')),
            ],
            options={
                u'ordering': ['position'],
                u'verbose_name': u'Host admin button',
                u'verbose_name_plural': u'Host admin buttons',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HostSSHKey',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('host', models.ForeignKey(to='hosts.Host', to_field=u'id', verbose_name=u'Host')),
                ('key', models.ForeignKey(to='sshkeys.SSHKey', to_field=u'id', verbose_name=u'Key')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Added by')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
            ],
            options={
                u'verbose_name': u'Host SSH key',
                u'verbose_name_plural': u'Host SSH keys',
            },
            bases=(models.Model,),
        ),
    ]
