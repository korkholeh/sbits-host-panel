# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '0003_ftpuser'),
    ]

    operations = [
        migrations.CreateModel(
            name='HostAdminButton',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('button_label', models.CharField(max_length=100, verbose_name=u'Button label')),
                ('button_class', models.CharField(max_length=100, verbose_name=u'Button label')),
                ('tooltip', models.TextField(verbose_name=u'Tooltip', blank=True)),
                ('commands', models.TextField(verbose_name=u'Commands', blank=True)),
                ('run_with_sudo', models.BooleanField(default=False, verbose_name=u'Run with sudo')),
                ('new_group', models.BooleanField(default=False, verbose_name=u'New group')),
                ('position', models.IntegerField(default=1, verbose_name=u'Position')),
                ('hosts', models.ManyToManyField(to='hosts.Host', verbose_name=u'Hosts')),
            ],
            options={
                u'ordering': ['position'],
                u'verbose_name': u'Host admin button',
                u'verbose_name_plural': u'Host admin buttons',
            },
            bases=(models.Model,),
        ),
    ]
