# encoding: utf8
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '0004_hostadminbutton'),
        ('sshkeys', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='HostSSHKey',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('host', models.ForeignKey(to='hosts.Host', to_field=u'id', verbose_name=u'Host')),
                ('key', models.ForeignKey(to='sshkeys.SSHKey', to_field=u'id', verbose_name=u'Key')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Added by')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
            ],
            options={
                u'verbose_name': u'Host SSH key',
                u'verbose_name_plural': u'Host SSH keys',
            },
            bases=(models.Model,),
        ),
    ]
