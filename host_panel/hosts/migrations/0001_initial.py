# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ServerType',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=u'Name')),
                ('build_script', models.TextField(verbose_name=u'Build script')),
            ],
            options={
                u'verbose_name': u'Server type',
                u'verbose_name_plural': u'Server types',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Host',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=u'Name')),
                ('server_type', models.ForeignKey(to='hosts.ServerType', to_field=u'id', verbose_name=u'Server type')),
                ('ip_address', models.IPAddressField(verbose_name=u'IP Address')),
                ('host_domain', models.CharField(default='my-host.com', max_length=200, verbose_name=u'Host domain')),
                ('description', models.TextField(help_text=u'You can use Markdown.', verbose_name=u'Description', blank=True)),
                ('main_user', models.CharField(max_length=50, verbose_name=u'Main user')),
                ('is_manageable', models.BooleanField(default=False, verbose_name=u'Is manageable')),
                ('sudo_password', models.CharField(max_length=50, verbose_name=u'Sudo password')),
            ],
            options={
                u'ordering': ['name'],
                u'verbose_name': u'Host',
                u'verbose_name_plural': u'Hosts',
            },
            bases=(models.Model,),
        ),
    ]
