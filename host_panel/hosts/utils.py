from fabric.api import env
from fabric.api import run, sudo


def add_sshkey_to_host(host_obj, sshkey):
    host_string = "%s@%s" % (host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    run('echo "%s" >> ~/.ssh/authorized_keys' % sshkey.key.key)


def remove_sshkey_from_host(host_obj, sshkey):
    host_string = "%s@%s" % (host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    run('sed -i \'0,/%s/{//d;}\' ~/.ssh/authorized_keys' % (
        sshkey.key.key.replace('/', '\/'),
        ))


def run_commands_on_host(domain, user, password, commands, run_with_sudo):
    host_string = "%s@%s" % (user, domain)
    env.host_string = host_string
    env.password = password
    if run_with_sudo:
        return sudo(commands)
    else:
        return run(commands)


def add_ftp_user(ftpuser_obj):
    host_string = "%s@%s" % (
        ftpuser_obj.host.main_user,
        ftpuser_obj.host.ip_address)
    env.host_string = host_string
    env.password = ftpuser_obj.host.sudo_password
    sudo('useradd --home /home/{user} --gid nogroup -m --shell /bin/false {user}'.format(
        user=ftpuser_obj.username))
    sudo("echo '{user}:{password}'|sudo chpasswd".format(
        user=ftpuser_obj.username,
        password=ftpuser_obj.password))
    sudo('echo "%s" >> /etc/vsftpd/vsftpd-virtual-user/vsftpd_user' % ftpuser_obj.username)

    user_vsftpd_file = '''
    local_root=/home/{user}

    cmds_allowed=USER,PASS,SYST,FEAT,OPTS,PWD,TYPE,PASV,LIST,STOR,CWD,MKD,SIZE,MDTM,CDUP,RETR,RNFR,RNTO

    local_umask=022

    write_enable=YES
    '''.format(user=ftpuser_obj.username)
    sudo('echo "{content}" > /etc/vsftpd/vsftpd-virtual-user/{user}'.format(
        content=user_vsftpd_file,
        user=ftpuser_obj.username))


def remove_ftp_user(ftpuser_obj):
    host_string = "%s@%s" % (
        ftpuser_obj.host.main_user,
        ftpuser_obj.host.ip_address)
    env.host_string = host_string
    env.password = ftpuser_obj.host.sudo_password
    sudo('rm -f /etc/vsftpd/vsftpd-virtual-user/{user}'.format(
        user=ftpuser_obj.username))
    sudo('sed -i \'0,/%s/{//d;}\' /etc/vsftpd/vsftpd-virtual-user/vsftpd_user' % (
        ftpuser_obj.username,
    ))
    # We do not delete user home directory
    sudo('userdel {user}'.format(user=ftpuser_obj.username))


def sync_cron_tasks(host_obj):
    host_string = "%s@%s" % (
        host_obj.main_user,
        host_obj.ip_address)
    env.host_string = host_string
    env.password = host_obj.sudo_password
    home_dir = '/home/%s' % host_obj.main_user
    tmp_file = '%s/cron.txt' % home_dir

    cron_tasks = host_obj.cron_tasks.all()
    run('touch %s' % tmp_file)
    for t in cron_tasks:
        task = '%s %s' % (t.cron_time, t.command)
        if not t.active:
            task = '#' + task
        run('echo \'%s\' >> %s' % (task, tmp_file))
    sudo('crontab %s' % tmp_file)
    run('rm -f %s' % tmp_file)
