# coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import (
    ServerType,
    Host,
    HostSSHKey,
    HostAdminButton,
    FTPUser,
    CronTask,
)
from .forms import ServerTypeForm


class ServerTypeAdmin(admin.ModelAdmin):
    list_display = ['name']
    form = ServerTypeForm

admin.site.register(ServerType, ServerTypeAdmin)


class HostAdminButtonAdmin(admin.ModelAdmin):
    list_display = [
        'button_label', 'tooltip',
        'run_with_sudo',
        'new_group', 'position']
    list_editable = ['new_group', 'position']

admin.site.register(HostAdminButton, HostAdminButtonAdmin)


class HostSSHKeyInline(admin.TabularInline):
    model = HostSSHKey
    raw_id_fields = ('key', 'added_by')
    extra = 0


class CronTaskInline(admin.TabularInline):
    model = CronTask
    raw_id_fields = ('added_by',)
    extra = 0


class FTPUserAdmin(admin.ModelAdmin):
    list_display = ('username', 'password', 'host', 'owner', 'added')
    raw_id_fields = ('owner',)

admin.site.register(FTPUser, FTPUserAdmin)


class HostAdmin(admin.ModelAdmin):
    list_display = ['name', 'ip_address', 'server_type']
    search_fields = ['name', 'ip_address']
    inlines = [
        HostSSHKeyInline,
        CronTaskInline,
    ]
    actions = ['sync_containers']

    def sync_containers(self, request, queryset):
        for q in queryset:
            q.sync_containers()
    sync_containers.short_description = _(u'Sync containers')

admin.site.register(Host, HostAdmin)


class CronTaskAdmin(admin.ModelAdmin):
    search_fields = ['command']
    list_display = [
        'cron_time', 'command', 'active', 'host',
        'added_by', 'modified']
    list_filter = ['host', 'active']
    raw_id_fields = ('added_by',)

admin.site.register(CronTask, CronTaskAdmin)
