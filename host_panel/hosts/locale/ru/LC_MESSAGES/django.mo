��    |      �  �   �      x
  Q   y
  p   �
  g   <  `   �  �        �     �     �     �     �     �               /     G     P     X     a     g     p  7   |  6   �  5   �  =   !     _     l     y     �     �     �  
   �  	   �     �  +   �  	   �  
   �  	                  !     *     ;     C     J     Z     i     x     �     �     �     �     �     �  	   �  	   �  #   �  	   �               $     )     .     ;     I     [     n  	   z     �     �  
   �  
   �     �     �     �  	   �     �     �  )   �  	        '     7     O     U     ^     c     l     �  &   �     �     �     �     �     �     �               !     .     3     9     G  I   W     �     �  "   �     �  "        ;     U     m     �     �     �  !   �     �     �  	   �     �  K        P  0   f  B   �  ]   �  �  8  ~     �   �  �   n  �     ]  �  L        c  "   t  -   �  4   �     �  $     "   =  )   `     �     �     �     �     �     �  W     b   k  R   �  W   !     y     �     �     �  "   �     �               /  h   D     �     �     �     �     �           +      ?      W   +   f      �      �       �      �      !  
   !     #!     0!     =!     [!     x!  P   �!     �!     �!  !   "     -"     <"     E"     ]"  :   y"  <   �"     �"     #  0   !#  
   R#     ]#     k#     y#     �#     �#  '   �#     �#     �#  P   $     a$     y$  <   �$     �$     �$     �$     �$      %  #   &%  S   J%      �%     �%  F   �%     "&     1&     L&     g&     v&     �&     �&     �&     �&  5   �&  �   '  '   �'  &   �'  =   �'  (   $(  +   M(  $   y(  "   �(  <   �(     �(     )     )  Y   6)     �)  5   �)     �)  #   �)  �   #*  L   �*  w   +  g   {+  �   �+     7          X   G       |      d      )   +   ,   ;       4              "              =                     u   W      b          5   '   6       `   j   w   K           ?          @   B   z   %   V   ^   9       :   l   <          E          a            q   	      m   t              \           g   >   1       #   c   k   s   D      0      &       _           P       I   {   T   !      F      Z   R      (   U       M   v   y          ]       Q   r   x       [      S   $   e          L   3       h   
          /   f       H   N   *   n           .           8   Y       O   J          C           2   o   -   A      p   i        
<p>Now paste it to the file with <code>Ctrl+Shift+V</code> and press Enter:</p>
 
<p>Now you have the ready new host for your infrastructure.
Press the button below and continue the setup.</p>
 
<p>Now you need to go to the user home directory and download the script
for auto setup the host:</p>
 
<p>Then use <code>Ctrl+O</code> for save and <code>Ctrl+X</code> for exit from the editor.</p>
 
<p>You have to make some preparations for making the new host manageable.</p>

<p>Firstly, open terminal with <code>Ctrl+Alt+T</code> and use the next command for login:</p>
 Access info for admins Active Add Cron task Add FTP user Add FTP user for Add SSH key Add SSH key for Add database server Add database server for Add host Add key Add task Added Added by Application Are you sure you want to delete Cron task "%(object)s"? Are you sure you want to delete FTP user "%(object)s"? Are you sure you want to delete SSH key "%(object)s"? Are you sure you want to delete database server "%(object)s"? Build script Button label Cancel Command Command for login Commands Connection Container Continue Copy the next public key to your clipboard: Cron task Cron tasks Cron time DB databases DB name DB users Database servers Default Delete Delete FTP user Delete SSH Key Delete SSH key Delete database server Delete task Description Domain Domains Edit FTP User FTP Users FTP users Give permission and run the script: Go to FTP Hide Hipache is not installed Home Host Host SSH key Host SSH keys Host admin button Host admin buttons Host domain Host list Host management commands Hosts IP Address IP address Install Hipache Is manageable Key Main user Modified Name Name of ftp user must starts with "ftp-". New group No description. Open the file for edit: Owner Password Port Position PostgreSQL databases PostgreSQL users Prepare to add the authorized ssh key: Public IP address Public port Pull the docker images: Remove Routing Run with sudo SSH keys Server type Server types Show Stats Sudo password Sync containers The setup of the host is not complete yet. Follow the instructions below. There are no Cron tasks yet. There are no FTP users yet. There are no PostgreSQL users yet. There are no SSH keys yet. There are no database servers yet. There are no domains yet. There are no hosts yet. This key already added to host. Tooltip Type Update Hipache DB Use the next password when asked: User User already exists. User name View setup script Wait while setup script prepare your host. It may take a couple of minutes. You can use Markdown. You can use that command for login to container: You don't have installed Hipache on this host yet. Install it now. minute (0-59), hour (0-23, 0 = midnight), day (1-31), month (1-12), weekday (0-6, 0 = Sunday) Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-06-18 11:49+0300
PO-Revision-Date: 2014-06-18 11:55+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 
<p>Теперь вставьте его в файл с помощью <code>Ctrl+Shift+V</code> и нажмите Enter:</p>
 
<p>Теперь ваш новый хост полностью готов для работы в составе инфраструктуры.
Нажмите на кнопку внизу и продолжите настройку.</p>
 
<p>Теперь, перейдите в домашний каталог и скачайте скрипт
автоматической настройки хоста:</p>
 
<p>Далее, используйте <code>Ctrl+O</code> для сохранения и <code>Ctrl+X</code> для выхода из редактора.</p>
 
<p>Вы должны совершить некоторые подготовительные действия чтобы сделать хост управляемым.</p>

<p>Сперва откройте терминал с помощью <code>Ctrl+Alt+T</code> и используйте следующую команду для входа:</p>
 Информация о доступе для администраторов Активный Добавить задачу Cron Добавить FTP пользователя Добавить FTP пользователя для Добавить SSH ключ Добавить SSH ключ для Добавить сервер БД Добавить сервер БД для Добавить хост Добавить ключ Добавить задачу Добавлено Добавил Приложение Вы уверены, что хотите удалить задачу Cron "%(object)s"? Вы уверены, что хотите удалить FTP пользователя "%(object)s"? Вы уверены, что хотите удалить SSH ключ "%(object)s"? Вы уверены, чт охотите удалить сервер БД "%(object)s"? Скрипт сборки Заголовок кнопки Отменить Команда Команда для логина Команды Подключение Контейнер Продолжить Скопируйте следующий публичный ключ в свой буфер обмена: Задача Cron Задачи Cron Время Cron Базы данных Название БД Пользователи БД Сервера БД По умолчанию Удалить Удалить FTP пользователя Удалить SSH ключ Удалить SSH ключ Удалить сервер БД Удалить задачу Описание Домен Домены Правка FTP пользователь  FTP пользователи FTP пользователи Дайте необходимые права и запустите скрипт: Перейти на FTP Скрыть Hipache не установлен Главная Хост Ключ SSH хоста Ключи SSH хостов Кнопки администрирования хоста Кнопки администрирования хостов Домен хоста Список хостов Команды управления хостом Хосты IP адрес IP адрес Установить Hipache Обслуживаемый Ключ Главный пользователь Модифицировано Название Имя пользователя ftp должно начинаться с "ftp-". Новая группа Без описания. Откройте файл на редактирование: Владелец Пароль Порт Позиция Базы данных PostgreSQL Пользователи PostgreSQL Подготовка к добавлению доверенного ssh ключа: Публичный IP адрес Публичный порт Скачайте образы контейнеров для Docker'а: Удалить Маршрутизация Запустить с sudo SSH ключи Тип сервера Типы серверов Показать Статистика Пароль sudo Синхронизировать контейнеры Настройка хочта еще не завершена. Следуйте указанным ниже инструкциям. Пока еще нет задач Cron. Нет FTP пользователей. Пока еще нет пользователей PostgreSQL. Пока еще нет SSH ключей. Пока ще нет серверов БД. Пока еще нет хостов. Пока ще нет хостов. Данный ключ уже добавлен к хосту. Подсказка Тип Обновить БД Hipache Используйте следующий пароль, если будет вопрос: Пользователь Пользователь уже существует. Имя пользователя Просмотреть скрипт Подождите пока скрипт подготовит ваш хост. Это может занять определенное время. Вы можете использовать Markdown для разметки. Вы можете использовать следующую команду для логина в контейнер: На хосте отсутствует установленный Hipache. Установите его. минуты (0-59), часы (0-23, 0 = полночь), часы (1-31), месяцы (1-12), дни недели (0-6, 0 = воскресенье) 