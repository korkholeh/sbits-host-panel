��    |      �  �   �      x
  Q   y
  p   �
  g   <  `   �  �        �     �     �     �     �     �               /     G     P     X     a     g     p  7   |  6   �  5   �  =   !     _     l     y     �     �     �  
   �  	   �     �  +   �  	   �  
   �  	                  !     *     ;     C     J     Z     i     x     �     �     �     �     �     �  	   �  	   �  #   �  	   �               $     )     .     ;     I     [     n  	   z     �     �  
   �  
   �     �     �     �  	   �     �     �  )   �  	        '     7     O     U     ^     c     l     �  &   �     �     �     �     �     �     �               !     .     3     9     G  I   W     �     �  "   �     �  "        ;     U     m     �     �     �  !   �     �     �  	   �     �  K        P  0   f  B   �  ]   �  �  8  �     �   �  �   y  �   8  Z  �  @   8     y     �  '   �  .   �     �           9  %   X     ~     �     �     �  
   �     �  Y   �  b   I  T   �  [        ]     w     �     �      �     �     �     �       [   "     ~     �     �     �     �     �     �              +   /      [      y   "   �      �      �   
   �      �      �      !     !     :!  ?   U!     �!     �!  #   �!     �!     �!     �!     "  6   0"  8   g"     �"     �"  .   �"  
   �"     
#     #     *#     G#     d#  %   m#     �#  
   �#  S   �#     $     $  6   2$     i$     x$     �$     �$     �$  !   �$  N   �$      -%     N%  )   j%     �%     �%     �%     �%     �%      &     &     +&     @&  1   R&     �&      '  -   %'  4   S'  !   �'  &   �'     �'     �'  8   (     G(     X(     _(  b   {(     �(  '   �(      )  8   <)  �   u)  :   *  y   F*  h   �*  �   )+     7          X   G       |      d      )   +   ,   ;       4              "              =                     u   W      b          5   '   6       `   j   w   K           ?          @   B   z   %   V   ^   9       :   l   <          E          a            q   	      m   t              \           g   >   1       #   c   k   s   D      0      &       _           P       I   {   T   !      F      Z   R      (   U       M   v   y          ]       Q   r   x       [      S   $   e          L   3       h   
          /   f       H   N   *   n           .           8   Y       O   J          C           2   o   -   A      p   i        
<p>Now paste it to the file with <code>Ctrl+Shift+V</code> and press Enter:</p>
 
<p>Now you have the ready new host for your infrastructure.
Press the button below and continue the setup.</p>
 
<p>Now you need to go to the user home directory and download the script
for auto setup the host:</p>
 
<p>Then use <code>Ctrl+O</code> for save and <code>Ctrl+X</code> for exit from the editor.</p>
 
<p>You have to make some preparations for making the new host manageable.</p>

<p>Firstly, open terminal with <code>Ctrl+Alt+T</code> and use the next command for login:</p>
 Access info for admins Active Add Cron task Add FTP user Add FTP user for Add SSH key Add SSH key for Add database server Add database server for Add host Add key Add task Added Added by Application Are you sure you want to delete Cron task "%(object)s"? Are you sure you want to delete FTP user "%(object)s"? Are you sure you want to delete SSH key "%(object)s"? Are you sure you want to delete database server "%(object)s"? Build script Button label Cancel Command Command for login Commands Connection Container Continue Copy the next public key to your clipboard: Cron task Cron tasks Cron time DB databases DB name DB users Database servers Default Delete Delete FTP user Delete SSH Key Delete SSH key Delete database server Delete task Description Domain Domains Edit FTP User FTP Users FTP users Give permission and run the script: Go to FTP Hide Hipache is not installed Home Host Host SSH key Host SSH keys Host admin button Host admin buttons Host domain Host list Host management commands Hosts IP Address IP address Install Hipache Is manageable Key Main user Modified Name Name of ftp user must starts with "ftp-". New group No description. Open the file for edit: Owner Password Port Position PostgreSQL databases PostgreSQL users Prepare to add the authorized ssh key: Public IP address Public port Pull the docker images: Remove Routing Run with sudo SSH keys Server type Server types Show Stats Sudo password Sync containers The setup of the host is not complete yet. Follow the instructions below. There are no Cron tasks yet. There are no FTP users yet. There are no PostgreSQL users yet. There are no SSH keys yet. There are no database servers yet. There are no domains yet. There are no hosts yet. This key already added to host. Tooltip Type Update Hipache DB Use the next password when asked: User User already exists. User name View setup script Wait while setup script prepare your host. It may take a couple of minutes. You can use Markdown. You can use that command for login to container: You don't have installed Hipache on this host yet. Install it now. minute (0-59), hour (0-23, 0 = midnight), day (1-31), month (1-12), weekday (0-6, 0 = Sunday) Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-06-18 11:49+0300
PO-Revision-Date: 2014-06-20 09:45+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 
<p>Далі вставте його у файл з допомогою <code>Ctrl+Shift+V</code> та натисніть Enter:</p>
 
<p>Тепер ви маєте готовий хост для використання у вашій інфраструктурі.
Натисніть на кнопку нижче для продовження налаштування.</p>
 
<p>Тепер ви маєте перейти в домашній каталог та завантажити скрипт
автоматичного налаштування хосту:</p>
 
<p>Потім використовуйте <code>Ctrl+O</code> для збережання та <code>Ctrl+X</code> для виходу з редактору.</p>
 
<p>Ви маєте зробити певні приготування, щоб мати можливість обслуговувати даний хост.</p>

<p>Спершу, відкрийте термінал з допомогою <code>Ctrl+Alt+T</code> та використайте наступну команду для входу:</p>
 Інформація по доступам для адмінів Активно Додати задачу Cron Додати FTP користувача Додати користувача FTP для Додати SSH ключ Додати SSH ключ для Додати сервер БД Додати сервер БД для Додати хост Додати ключ Додати задачу Додано Додав Аплікація Ви впевнені, що хочете видалини задачу Cron "%(object)s"? Ви впевнені, що хочете видалити користувача FTP "%(object)s"? Ви впевнені, що хочете видалити SSH ключ "%(object)s"? Ви впевнені, що хочете видалити базу даних "%(object)s"? Скрипт збірки Підпис кнопки Скасувати Команда Команда для входу Команди Підключення Контейнер Продовжити Копіюємо наступний публічний ключ в буфер обміну: Задача Cron Задачі Cron Час Cron Бази даних Назва БД Користувачі БД Сервери БД За замовченням Видалити Видалити користувача FTP Видалити SSH ключ Видалити SSH ключ Видалити сервер БД Видалити задачу Опис Домен Домени Правка Користувач FTP Користувачі FTP Користувачі FTP Надайте права та запустіть скрипт: Перейти на FTP Приховати Hipache не встановлено Головна Хост SSH ключ хосту SSH ключі хостів Кнопка адміністрування хосту Кнопки адміністрування хостів Домен хосту Список хостів Команди керування хостом Хости IP адреса IP адреса Встановити Hipache Обслуговується Ключ Головний користувач Виправлено Назва Ім’я користувача ftp повинно починатися з "ftp-". Нова група Немає опису. Відкрити файл на редагування: Власник Пароль Порт Позиція Бази даних PostgreSQL Користувачі PostgreSQL Готуємо додавання авторизованого SSH ключа: Публічна IP адреса Публічний порт Завантажте образи Docker: Видалити Маршрутизація Запустити з sudo SSH ключі Тип серверу Типи серверів Показати Статистика Пароль sudo Синхронізація контейнерів Налаштування хосту ще не закінчено. Використовуйте інструкції внизу. Ще немаж задач Cron. Ще немає користувачів FTP. Ще немає користувачів PostgreSQL. Ще немає SSH ключів. Ще немає серверів БД. Ще немає доменів Ще немає хостів. Даний ключ вже додано до хосту. Підказка Тип Оновити БД Hipache Використовуйте наступний пароль якщо буде запитання: Користувач Користувач вже існує. Ім’я користувача Перегляд скрипта налаштування Почекайте доки скрипт налаштування підготує ваш хост. Це може зайняти певний час. Ви можете використовувати Markdown. Ви можете використовувати наступну команду для входу в контейнер: Hipache ще не встановлено на цей хост. Встановіть його зараз. хвилини (0-59), години (0-23, 0 = північ), дні (1-31), місяці (1-12), дні тижня (0-6, 0 = неділя) 