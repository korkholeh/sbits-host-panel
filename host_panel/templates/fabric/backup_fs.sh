#!/bin/bash
rm -f $(ls -1t {{backup_settings.directory}}/{{backup_settings.webapp.name}}-fs_dump-*.tar.gz | tail -n +{{backup_settings.keep_fs_files}})
docker export {{backup_settings.webapp.docker_container.container_id}} | gzip > {{backup_settings.directory}}/{{backup_settings.webapp.name}}-fs_dump-`date +%Y-%m-%d-%H:%M`.tar.gz
