#!/bin/bash
{% for db in dbs %}
rm -f $(ls -1t {{backup_settings.directory}}/{{backup_settings.webapp.name}}-db_dump-{{db.name}}-*.gz | tail -n +{{backup_settings.keep_db_files}})
pg_dump "dbname={{db.name}} host={{db.pg_service.host.ip_address}} user={{db.username}} password={{db.pg_user.password}} port={{db.pg_service.public_service_port}}" | gzip > {{backup_settings.directory}}/{{backup_settings.webapp.name}}-db_dump-{{db.name}}-`date +%Y-%m-%d-%H:%M`.gz
{% endfor %}
