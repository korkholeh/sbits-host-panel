# encoding: utf8
from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name=u'password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name=u'last login')),
                ('is_superuser', models.BooleanField(default=False, help_text=u'Designates that this user has all permissions without explicitly assigning them.', verbose_name=u'superuser status')),
                ('email', models.EmailField(unique=True, max_length=255, verbose_name=u'Email')),
                ('full_name', models.CharField(max_length=100, verbose_name=u'Full name')),
                ('language', models.CharField(default='en', max_length=10, verbose_name=u'Language', choices=[('en', u'English'), ('ru', u'Russian'), ('uk', u'Ukrainian')])),
                ('is_admin', models.BooleanField(default=False, verbose_name=u'Admin status')),
                ('is_active', models.BooleanField(default=True, verbose_name=u'Active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name=u'Date joined')),
                ('groups', models.ManyToManyField(to='auth.Group', verbose_name=u'groups', blank=True)),
                ('user_permissions', models.ManyToManyField(to='auth.Permission', verbose_name=u'user permissions', blank=True)),
            ],
            options={
                u'verbose_name': u'Customer',
                u'verbose_name_plural': u'Customers',
            },
            bases=(models.Model,),
        ),
    ]
