��    !      $  /   ,      �  &   �  )        :     C     J     W     `  	   q  $   {     �     �     �  &   �  3   �  ]     	   x     �     �     �     �     �  
   �     �     �     �     �  �   �     �     �  %   �  "   �  :   �  �    W   �  A   C     �     �  )   �     �  4   �     (	  N   ?	     �	  .   �	     �	  L   �	  C   H
  �   �
     C     X     g          �  
   �  $   �     �  (   �  
   �  $       +     K     Z  )   k  S   �  �   �                                           
                                                        	                        !                                    A user with that email already exists. A user with that nickname already exists. Accounts Active Admin status Customer Customer account Customers Customers must have an email address Date joined Edit account Email Enter an email, nickname and password. Enter the same password as above, for verification. First, enter an email, nickname and password. Then, you'll be able to edit more user options. Full name General Important dates Language Log in Log out My account Password Password confirmation Permissions Please log in Raw passwords are not stored, so there is no way to see this user's password, but you can change the password using <a href="password/">this form</a>. SSH keys Save The two password fields didn't match. Your account settings was changed. Your username and password didn't match. Please try again. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-06-16 11:09+0300
PO-Revision-Date: 2014-06-16 11:36+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 Користувач з такою елетронною поштою вже існує. Користувач з таким ім’ям вже існує. Облікові записи Активний Статус адміністратора Користувач Обліковий запис користувача Користувачі Користувач повинен мати електронну адресу Дата реєстрації Правка облікового запису Електронна пошта Введіть електронну пошту, ім’я та пароль. Введіть пароль ще раз, для перевірки. Спершу введіть електронну адресу, ім’я та пароль. Потім матимете можливість редагувати інші опції. Повне ім’я Головна Важливі дати Мова Увійти Війти Мій обліковий запис Пароль Підтвердження пароля. Права Будь-ласка увійдіть Паролі не зберігаються в чистому вигляді, тому немаж можливості побачити пароль користувачв, але ви можете його змінити з допомогою <a href="password/">цієї форми</a>. SSH ключі Зберегти Паролі не співпадають. Налаштування облікового запису були змінені. Ім’я користувача та пароль не відповідають одне одному. Будь-ласка спробуйте ще. 