��    !      $  /   ,      �  &   �  )        :     C     J     W     `  	   q  $   {     �     �     �  &   �  3   �  ]     	   x     �     �     �     �     �  
   �     �     �     �     �  �   �     �     �  %   �  "   �  :   �  �    g   �  P   S     �     �  )   �     �  4   	     G	  ]   `	     �	  (   �	  !   
  I   )
  O   s
  �   �
     s     �     �     �  
   �  
   �  "   �     �  '   �  
   #  #   .  �   R     D     S  %   f  8   �  h   �                                           
                                                        	                        !                                    A user with that email already exists. A user with that nickname already exists. Accounts Active Admin status Customer Customer account Customers Customers must have an email address Date joined Edit account Email Enter an email, nickname and password. Enter the same password as above, for verification. First, enter an email, nickname and password. Then, you'll be able to edit more user options. Full name General Important dates Language Log in Log out My account Password Password confirmation Permissions Please log in Raw passwords are not stored, so there is no way to see this user's password, but you can change the password using <a href="password/">this form</a>. SSH keys Save The two password fields didn't match. Your account settings was changed. Your username and password didn't match. Please try again. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-06-16 11:09+0300
PO-Revision-Date: 2014-06-16 11:10+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 Пользователь с такой электронной почтой уже существует. Пользователь с таким именем уже существует. Учетные записи Активен Статус администратора Пользователь Учетная запись пользователя Пользователи Пользователь должен иметь адрес электронной почты Дата регистрации Правка учетной записи Электронная почта Введите электронную почту, имя и пароль. Введите тот же пароль еще раз, для проверки. Сначала введите электронную почту, имя и пароль. Далее вы сможете отредактировать другие опции. Полное имя Главная Важные даты Язык Войти Выйти Моя учетная запись Пароль Подтверждение пароля Права Пожалуйста войдите Пароли не хранятся в чистом виде, поэтому нет способа его увидеть, но вы можете изменить пароль используя <a href="password/">данную форму</a>. SSH ключи Сохранить Пароли не совпадают. Ваша учетная запись обновлена. Имя пользователя или пароль не верны. Попробуйте еще раз. 