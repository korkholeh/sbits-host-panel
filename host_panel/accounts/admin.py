# coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin

from .models import Customer
from .forms import CustomerChangeForm, CustomerCreationForm


class CustomerAdmin(UserAdmin):
    add_form_template = 'accounts/admin/auth/user/add_form.html'
    fieldsets = (
        (None, {
            'fields': (
                'email', 'full_name',
                'password',
            )}),
        (_(u'Permissions'), {
            'fields': (
                'is_active', 'is_admin',
                'is_superuser',
                'groups',
                'user_permissions',
            )}),
        (_(u'Important dates'), {
            'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'full_name', 'password1', 'password2')
        }),
    )

    list_display = ('email', 'full_name', 'is_admin', 'is_active')
    list_filter = ('is_admin', 'is_superuser', 'is_active', 'groups')
    search_fields = ('full_name', 'email')
    ordering = ('full_name',)
    date_hierarchy = 'date_joined'
    filter_horizontal = ('groups', 'user_permissions',)

    form = CustomerChangeForm
    add_form = CustomerCreationForm

admin.site.register(Customer, CustomerAdmin)
