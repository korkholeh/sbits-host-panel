# coding: utf-8
from django.shortcuts import render
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.views.generic.detail import DetailView
from django.views.generic import CreateView, UpdateView, DeleteView
from django.views.generic.edit import FormView
from django.contrib.messages.views import SuccessMessageMixin
from .models import Customer
from .forms import EditAccountForm


class CustomerDetailView(DetailView):
    model = Customer
    context_object_name = 'customer'


class MyAccountUpdateView(SuccessMessageMixin, UpdateView):
    model = Customer
    template_name = 'accounts/edit_my_account.html'
    form_class = EditAccountForm
    success_message = _(u"Your account settings was changed.")

    def get_object(self):
        return Customer.objects.get(pk=self.request.user.pk)

    def get_success_url(self):
        return reverse(
            'customer_details', args=(self.request.user.pk,))
