# coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit
from .models import Customer


class CustomerChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(
        label=_(u'Password'),
        help_text=_(
            u'Raw passwords are not stored, so there is no way to see '
            u'this user\'s password, but you can change the password '
            u'using <a href="password/">this form</a>.'))

    class Meta:
        model = Customer
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(CustomerChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        return self.initial['password']


class CustomerCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges,
    from the given email, full_name and password.
    """
    error_messages = {
        'duplicate_email': _(u"A user with that email already exists."),
        'duplicate_nickname': _(u"A user with that nickname already exists."),
        'password_mismatch': _(u"The two password fields didn't match."),
    }
    password1 = forms.CharField(
        label=_(u"Password"),
        widget=forms.PasswordInput)
    password2 = forms.CharField(
        label=_(u"Password confirmation"),
        widget=forms.PasswordInput,
        help_text=_(u"Enter the same password as above, for verification."))

    class Meta:
        model = Customer
        fields = ('email', 'full_name',)

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            Customer.objects.get(email=email)
        except Customer.DoesNotExist:
            return email
        raise forms.ValidationError(self.error_messages['duplicate_email'])

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'])
        return password2

    def save(self, commit=True):
        customer = super(CustomerCreationForm, self).save(commit=False)
        customer.set_password(self.cleaned_data["password1"])
        if commit:
            customer.save()
        return customer


class EditAccountForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = (
            'email', 'full_name', 'language')

    def __init__(self, *args, **kwargs):
        super(EditAccountForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-lg-3'
        self.helper.field_class = 'col-lg-9'
        self.helper.layout = Layout(
            Fieldset(
                _(u'General'),
                'email',
                'full_name',
                'language',
            ),
            ButtonHolder(
                Submit('submit', _(u'Save'), css_class='btn btn-primary')
            )
        )
