# coding: utf-8
from django.conf.urls import patterns, url
from django.contrib.auth.views import login, logout
from .views import (
    CustomerDetailView,
    MyAccountUpdateView,
)


urlpatterns = patterns(
    '',
    url(
        r'^login/$', login,
        {'template_name': 'accounts/login.html'}, name='login'),
    url(r'^logout/$', logout, {'next_page': '/'}, name='logout'),
    url(
        r'^my-account/edit/$',
        MyAccountUpdateView.as_view(),
        name='edit_my_account'),
    url(
        r'^(?P<pk>\d+)/$', CustomerDetailView.as_view(),
        name='customer_details'),
)
