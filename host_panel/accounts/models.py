# coding: utf-8
from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)


class CustomerManager(BaseUserManager):
    """
    Creates and saves a Customer with the given email, full_name and password.
    """
    def create_user(self, email, full_name='', password=None):
        """
        Creates and saves a Customer with the given email,
        full_name and password.
        """
        if not email:
            raise ValueError(_(u'Customers must have an email address'))

        customer = self.model(
            email=CustomerManager.normalize_email(email),
            full_name=full_name
        )
        customer.set_password(password)
        customer.save(using=self._db)
        return customer

    def create_superuser(self, email, full_name='', password=None):
        """
        Creates and saves a superuser with the given email,
        full_name and password.
        """
        customer = self.create_user(email, full_name, password)
        customer.is_admin = True
        customer.save(using=self._db)
        return customer


class Customer(AbstractBaseUser, PermissionsMixin):
    """
    A model which implements the authentication model.

    Email, password and full_name are required. Other fields are optional.

    Email field is used for logging in.
    """
    email = models.EmailField(
        _(u'Email'), max_length=255, unique=True)
    full_name = models.CharField(
        _(u'Full name'), max_length=100)

    language = models.CharField(
        _(u'Language'), default=settings.LANGUAGE_CODE,
        max_length=10, choices=settings.LANGUAGES)

    is_admin = models.BooleanField(_(u'Admin status'), default=False)
    is_active = models.BooleanField(_(u'Active'), default=True)

    date_joined = models.DateTimeField(_(u'Date joined'), default=timezone.now)

    objects = CustomerManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['full_name']

    class Meta:
        verbose_name = _(u'Customer')
        verbose_name_plural = _(u'Customers')

    def __unicode__(self):
        return self.full_name or self.email

    def get_full_name(self):
        return self.full_name

    def get_short_name(self):
        return self.full_name

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin
