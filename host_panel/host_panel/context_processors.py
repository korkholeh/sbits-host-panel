# coding: utf-8
from django.conf import settings


def panel_settings(request):
    return {
        'HOST_PANEL_TITLE': settings.HOST_PANEL_TITLE,
        'HOST_PANEL_THEME': settings.HOST_PANEL_THEME,
    }
