# coding: utf-8
from .base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG

EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'chicago',
        'USER': 'postgres',
        'PASSWORD': '123456',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}

INSTALLED_APPS += ('debug_toolbar',)
INTERNAL_IPS = ('127.0.0.1',)
MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)

# HOST_PANEL_TITLE = u'Aquaweb Host Panel'
HOST_PANEL_THEME = 'lumen'

SHIPYARD_USER = 'admin'
SHIPYARD_API_KEY = '35cda37a73884859c8a4c8d773876d5786b9ad60'

MAINTAINABLE_SSH_KEY = 'ssh-dss AAAAB3NzaC1kc3MAAACBAJkL3MlqKhYMkuKFefVRiQ/RdqfoWYfDbPVoc81P/2BUjBziFWML7KWO4twExs0ycqygMRLa9a4a3Vgsygk+8p6FJXqtmvBevy5Z0wGKUlnM15jKSi61aWVxSGIUHDK6p47pTqSnhenCMhIS37VaZQQvDLuFApLN3B8fqUz+8pVlAAAAFQDcuSmo9Lb9L1EoiDa55EJAXtf3LwAAAIAFDJYHtc0HpEzMlKJWcIZGRyD6DbTkGYnNCrx7P7K2QGN8tpmD06nLbhSQlDQolVYDhwFfTkT3J4JG52cC0iVnwBUa9tuNCpmI6Opf8jhwcMxT/sPRUe6FLQdG21xI6He7juVZB1ePFD1+8SA75s2V0FtY1coLH0q0/f+ZmNba/wAAAIA7OtxrhJmnjiBoi5AvkmLp9WIcfaLvObOIhFnWXa7zmGm7i9OOOS3CMlX9O1t5gVj75NjyK/6prn3ld5LiG5C/4hq3nmwN8pQ8Yt5WIDJvbBUidV+A1fgWFOZSIu+RsZCI+h93z8scK/yirM3aeUAHwuIp4B/TEHfRWCX4QT8N3g== bum@thinkpad-t430'
