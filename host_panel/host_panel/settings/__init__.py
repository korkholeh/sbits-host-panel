import os
mode = os.getenv('WSGI_ENV', 'dev')

if mode == 'dev':
    from .local import *
elif mode == 'production':
    from .production import *
