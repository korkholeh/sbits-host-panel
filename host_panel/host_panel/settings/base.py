"""
Django settings for host_panel project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""
from django.utils.translation import ugettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), '..')


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '4*y(d=5klhgn+yzo%=x8r8(a&t)rwjr0k%nmkqd#%finkl6mu_'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'hvad',
    'crispy_forms',
    'django_select2',
    'accounts',
    'hosts',
    'containers',
    'services',
    'navigation',
    'sshkeys',
    'applications',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'host_panel.middleware.LoginRequiredMiddleware',
    'host_panel.middleware.CustomerLanguageMiddleware',
)

ROOT_URLCONF = 'host_panel.urls'

WSGI_APPLICATION = 'host_panel.wsgi.application'


# Database
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/

LANGUAGE_CODE = 'en'
LANGUAGES = (
    ('en', _(u'English')),
    ('ru', _(u'Russian')),
    ('uk', _(u'Ukrainian')),
)

TIME_ZONE = 'Europe/Kiev'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)
LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'host_panel'),
)

PUBLIC_DIR = os.path.join(BASE_DIR, '..', 'public')
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(PUBLIC_DIR, 'media')
STATIC_ROOT = os.path.join(PUBLIC_DIR, 'static')

AUTH_USER_MODEL = 'accounts.Customer'
LOGIN_REDIRECT_URL = '/'

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    'django.core.context_processors.request',
    'host_panel.context_processors.panel_settings',
)

MESSAGE_STORAGE = 'django.contrib.messages.storage.cookie.CookieStorage'

CRISPY_TEMPLATE_PACK = 'bootstrap3'
SELECT2_BOOTSTRAP = True

HOST_PANEL_TITLE = u'Grape'
HOST_PANEL_THEME = None

MAIN_CLOUD_DOMAIN = 'the7bits-cloud.com'
HIPACHI_DOCKER_IMAGE = 'korkholeh/hipache:0.3'

# Set here the ssh key from the your host panel container
MAINTAINABLE_SSH_KEY = ''
HOST_PANEL_DOMAIN = 'http://localhost:8000'

LOGIN_EXEMPT_URLS = [
    r'^hosts/\d+/host-setup.sh$',
]

FABRIC_TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates/fabric')

CODEMIRROR_PATH = 'vendor/codemirror'
CODEMIRROR_THEME = 'default'
