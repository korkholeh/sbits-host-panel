# coding: utf-8
from .base import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = [
    'control.grape-project.com',
    'localhost',
]

HOST_PANEL_THEME = 'lumen'

# Insert database settings here
