# coding: utf-8
from django.core.exceptions import PermissionDenied


class CheckOwnerMixin(object):
    def get_object(self, get_queryset=None):
        obj = super(CheckOwnerMixin, self).get_object()
        if not obj.owner == self.request.user:
            raise PermissionDenied()
        return obj
