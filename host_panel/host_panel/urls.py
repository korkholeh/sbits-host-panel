from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

urlpatterns = patterns(
    '',
    url(r'^$', 'host_panel.views.home', name='home'),
    url(r'^select2/', include('django_select2.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('accounts.urls')),
    url(r'^hosts/', include('hosts.urls', namespace='hosts')),
    url(r'^applications/', include('applications.urls', namespace='applications')),
    url(r'^sshkeys/', include('sshkeys.urls', namespace='sshkeys')),
)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns(
        '',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
