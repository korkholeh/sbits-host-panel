# coding: utf-8
from django.shortcuts import render_to_response, redirect
from django.core.urlresolvers import reverse
from django.template import RequestContext


def home(request):
    if not request.user.is_authenticated():
        return redirect(reverse('login'))

    return render_to_response(
        'home.html', RequestContext(request, {}))
