# coding: utf-8
from django.core.urlresolvers import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic import CreateView, UpdateView, DeleteView
from host_panel.mixins import CheckOwnerMixin
from .models import SSHKey
from .forms import SSHKeyForm


class SSHKeyListView(ListView):
    model = SSHKey
    context_object_name = 'keys'

    def get_queryset(self):
        user = self.request.user
        return SSHKey.objects.filter(owner=user)


class SSHKeyCreateView(CreateView):
    model = SSHKey
    form_class = SSHKeyForm
    success_url = reverse_lazy('sshkeys:list')

    def get_form_kwargs(self):
        kwargs = super(SSHKeyCreateView, self).get_form_kwargs()
        kwargs['owner'] = self.request.user
        return kwargs

    def form_valid(self, form):
        key = form.save(commit=False)
        key.owner = self.request.user
        key.save()
        return super(SSHKeyCreateView, self).form_valid(form)


class SSHKeyEditView(CheckOwnerMixin, UpdateView):
    model = SSHKey
    form_class = SSHKeyForm
    success_url = reverse_lazy('sshkeys:list')

    def get_form_kwargs(self):
        kwargs = super(SSHKeyEditView, self).get_form_kwargs()
        kwargs['owner'] = self.request.user
        return kwargs


class SSHKeyDeleteView(CheckOwnerMixin, DeleteView):
    model = SSHKey
    success_url = reverse_lazy('sshkeys:list')
