# coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import SSHKey


class SSHKeyForm(forms.ModelForm):

    class Meta:
        model = SSHKey
        fields = ['name', 'key']

    def __init__(self, owner, *args, **kwargs):
        super(SSHKeyForm, self).__init__(*args, **kwargs)
        self.owner = owner
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['key'].widget.attrs['readonly'] = True

    def clean_name(self):
        sshkeys = SSHKey.objects.filter(
            owner=self.owner, name=self.cleaned_data['name'])
        if sshkeys:
            raise forms.ValidationError(
                _(u'You already have SSH key with this name.'))
        else:
            return self.cleaned_data['name']

    def clean_key(self):
        sshkeys = SSHKey.objects.filter(
            owner=self.owner, key=self.cleaned_data['key'])
        if sshkeys and not self.fields['key'].widget.attrs['readonly']:
            raise forms.ValidationError(
                _(u'You already have this SSH key.'))
        else:
            return self.cleaned_data['key']
