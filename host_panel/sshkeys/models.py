# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class SSHKey(models.Model):
    name = models.CharField(_(u'Name'), max_length=100)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_(u'Owner'))
    key = models.TextField(_(u'Key'))
    added = models.DateTimeField(_(u'Added'), auto_now_add=True)

    class Meta:
        verbose_name = _(u'SSH Key')
        verbose_name_plural = _(u'SSH Keys')
        unique_together = ('name', 'owner', 'key')

    def __unicode__(self):
        return self.name
