��          �      |      �  �   �     �     �     �  5   �     �     �     �     �     	               3     7     <     B     J     S     \  (   w     �  �  �  t  �                8  R   K     �     �     �     �     �       .        N     W     h     y     �     �  ?   �  D   �  2   6	                                                     	           
                                 
<p>Host and container administration can work only with ssh keys.
You can generate own key on your machine with command:</p>
<pre>ssh-keygen -t dsa</pre>
 Add SSH Key Add key Added Are you sure you want to delete SSH key "%(object)s"? Cancel Delete Delete SSH Key Delete SSH key Edit Edit SSH key How to generate SSH key Key Name Owner SSH Key SSH Keys Save key There are no SSH keys yet. You already have SSH key with this name. You already have this SSH key. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-06-03 21:24+0300
PO-Revision-Date: 2014-06-03 21:30+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 
<p>Административный доступ, как к хостам так и к контейнерам, возможен только с использованием ssh ключей.
Вы можете сгенерировать собственный ключ на своем компьютере с помощью команды:</p>
<pre>ssh-keygen -t dsa</pre>
 Добавить SSH ключ Добавить ключ Добавлено Вы уверены, что хотите удалить SSH ключ "%(object)s"? Отменить Удалить Удалить SSH ключ Удалить SSH ключ Правка Правка SSH ключа Как сгенерировать SSH ключ Ключ Название Владелец SSH ключ SSH ключи Сохранить ключ Пока еще нет добавленных SSH ключей. SSH ключ с таким именем уже существует. Данный SSH ключ уже добавлен. 