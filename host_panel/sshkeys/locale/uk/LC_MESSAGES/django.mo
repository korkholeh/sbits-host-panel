��          �      |      �  �   �     �     �     �  5   �     �     �     �     �     	               3     7     <     B     J     S     \  (   w     �  �  �  M  �     �     �     	  T        k     ~     �     �     �     �  (   �       
   $     /     >     K     Z  !   t  ?   �  +   �                                                     	           
                                 
<p>Host and container administration can work only with ssh keys.
You can generate own key on your machine with command:</p>
<pre>ssh-keygen -t dsa</pre>
 Add SSH Key Add key Added Are you sure you want to delete SSH key "%(object)s"? Cancel Delete Delete SSH Key Delete SSH key Edit Edit SSH key How to generate SSH key Key Name Owner SSH Key SSH Keys Save key There are no SSH keys yet. You already have SSH key with this name. You already have this SSH key. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-06-20 09:53+0300
PO-Revision-Date: 2014-06-20 09:59+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 
<p>Адміністрування хостів та контейнерів можна виконувати лише з допомогою SSH ключів.
Ви можете згенерувати власний ключ на своєму комп’ютері з допомогою команди:</p>
<pre>ssh-keygen -t dsa</pre>
 Додати SSH ключ Додати ключ Додано Ви впевнені, що хочете видалити SSH ключ "%(object)s"? Скасувати Видалити Видалити SSH ключ Видалити SSH ключ Правка Правка SSH ключ Як згенерувати SSH ключ Ключ Назва Власник SSH ключ SSH ключі Зберегти ключ Ще немає SSH ключів. Ви вже маєте SSH ключ з такою назвою. Ви вже маєте цей SSH ключ. 