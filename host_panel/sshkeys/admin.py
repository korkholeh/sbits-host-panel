# coding: utf-8
from django.contrib import admin
from .models import (
    SSHKey,
)


class SSHKeyAdmin(admin.ModelAdmin):
    list_display = ['name', 'owner', 'added']

admin.site.register(SSHKey, SSHKeyAdmin)
