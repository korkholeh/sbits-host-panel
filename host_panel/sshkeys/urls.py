# coding: utf-8
from django.conf.urls import patterns, url
from .views import (
    SSHKeyListView,
    SSHKeyCreateView,
    SSHKeyEditView,
    SSHKeyDeleteView,
)


urlpatterns = patterns(
    'sshkeys.views',
    url(r'^add/$', SSHKeyCreateView.as_view(), name='add_key'),
    url(
        r'^(?P<pk>\d+)/delete/$',
        SSHKeyDeleteView.as_view(), name='delete_key'),
    url(
        r'^(?P<pk>\d+)/edit/$',
        SSHKeyEditView.as_view(), name='edit_key'),
    url(r'^$', SSHKeyListView.as_view(), name='list'),
)
