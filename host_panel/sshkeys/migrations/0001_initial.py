# encoding: utf8
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='SSHKey',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=u'Name')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Owner')),
                ('key', models.TextField(verbose_name=u'Key')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
            ],
            options={
                u'unique_together': set([('name', 'owner', 'key')]),
                u'verbose_name': u'SSH Key',
                u'verbose_name_plural': u'SSH Keys',
            },
            bases=(models.Model,),
        ),
    ]
