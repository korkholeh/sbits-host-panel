# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '__first__'),
        ('containers', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Hipache',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('host', models.OneToOneField(verbose_name=u'Host', to_field=u'id', to='hosts.Host')),
                ('docker_container', models.OneToOneField(null=True, to_field=u'id', blank=True, to='containers.Container', verbose_name=u'Container')),
                ('public_service_port', models.PositiveIntegerField(default=80, verbose_name=u'Public service port')),
                ('public_ssh_port', models.PositiveIntegerField(null=True, verbose_name=u'Public SSH port', blank=True)),
            ],
            options={
                u'verbose_name': u'Hipache instance',
                u'verbose_name_plural': u'Hipache instances',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PostgreSQLService',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=u'Name')),
                ('host', models.ForeignKey(to='hosts.Host', to_field=u'id', verbose_name=u'Host')),
                ('docker_container', models.OneToOneField(null=True, to_field=u'id', blank=True, to='containers.Container', verbose_name=u'Container')),
                ('postgres_password', models.CharField(max_length=40, verbose_name=u'Password for user "postgres"')),
                ('public_service_port', models.PositiveIntegerField(verbose_name=u'Public service port')),
                ('public_ssh_port', models.PositiveIntegerField(null=True, verbose_name=u'Public SSH port', blank=True)),
                ('is_default', models.BooleanField(default=False, verbose_name=u'Is default')),
            ],
            options={
                u'verbose_name': u'PostgreSQL DB Server',
                u'verbose_name_plural': u'PostgreSQL DB Servers',
            },
            bases=(models.Model,),
        ),
    ]
