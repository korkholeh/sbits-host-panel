# coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import (
    PostgreSQLService,
)
from containers.models import DockerImage


class AddPostgreSQLServer(forms.ModelForm):
    docker_image = forms.ModelChoiceField(
        queryset=DockerImage.objects.filter(is_database_server=True),
        label=_(u'Docker image'))

    class Meta:
        model = PostgreSQLService
        fields = [
            'name',
            'postgres_password',
            'is_default']
        widgets = {
            'postgres_password': forms.PasswordInput(),
        }

    def __init__(self, host, *args, **kwargs):
        super(AddPostgreSQLServer, self).__init__(*args, **kwargs)
        self.host = host

        try:
            max_pgs_id = \
                PostgreSQLService.objects.order_by('-id').first().id + 1
        except AttributeError:
            max_pgs_id = 1
        self.fields['name'].initial = 'postgresql_{0:02d}'.format(max_pgs_id)

    def clean_name(self):
        already_exist = PostgreSQLService.objects.filter(
            host=self.host,
            name=self.cleaned_data['name'],
        ).exists()

        if not already_exist:
            return self.cleaned_data['name']
        raise forms.ValidationError(_(u'Database server already exists.'))
