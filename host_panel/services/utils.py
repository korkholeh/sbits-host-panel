# coding: utf-8
import psycopg2
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from fabric.api import env
from fabric.api import run, sudo
import requests
import os


def postgresql_add_user(
        host, port, database, user, password,
        new_user, new_user_password):
    conn = psycopg2.connect(
        database=database,
        user=user,
        password=password,
        host=host,
        port=port)
    cur = conn.cursor()
    cur.execute('''CREATE ROLE {0} WITH LOGIN;
        ALTER USER {0} PASSWORD \'{1}\';
        '''.format(new_user, new_user_password))
    conn.commit()
    conn.close()


def postgresql_remove_user(
        host, port, database, user, password,
        username):
    conn = psycopg2.connect(
        database=database,
        user=user,
        password=password,
        host=host,
        port=port)
    cur = conn.cursor()
    cur.execute('DROP ROLE {0};'.format(username))
    conn.commit()
    conn.close()


def postgresql_add_database(
        host, port, database, user, password,
        db_name, db_user):
    conn = psycopg2.connect(
        database=database,
        user=user,
        password=password,
        host=host,
        port=port)
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cur = conn.cursor()
    cur.execute(
        'CREATE DATABASE {0} OWNER {1} ENCODING \'UTF-8\';'.format(
            db_name, db_user))
    cur.close()
    conn.close()


def postgresql_remove_database(
        host, port, database, user, password, db_name):
    conn = psycopg2.connect(
        database=database,
        user=user,
        password=password,
        host=host,
        port=port)
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    cur = conn.cursor()
    cur.execute('''SELECT pg_terminate_backend(pg_stat_activity.pid)
    FROM pg_stat_activity
    WHERE pg_stat_activity.datname = '{0}'
        AND pid <> pg_backend_pid();
    '''.format(db_name))
    cur.execute('DROP DATABASE {0};'.format(db_name))
    cur.close()
    conn.close()


# def add_sshkey_to_container(host_obj, container_name, key):
#     """Add SSH key to a docker container. Use Shipyard API for
#     find the necessary paths.
#     """
#     from hosts.models import ShipyardInstance
#     shipyard = ShipyardInstance.objects.filter(
#         hosts=host_obj).first()
#     print shipyard
#     r = requests.get(
#         '{domain}api/v1/containers/?username={user}&api_key={apikey}&limit=0'.format(
#             domain=shipyard.domain,
#             user=shipyard.username,
#             apikey=shipyard.api_key))
#     data = r.json()
#     container_objs = data['objects']
#     for obj in container_objs:
#         if obj['description'] == container_name:
#             volumes = obj['meta']['Volumes']
#             ssh_dir = volumes['/root/.ssh']
#             host_string = "%s@%s" % (
#                 host_obj.main_user, host_obj.ip_address)
#             env.host_string = host_string
#             env.password = host_obj.sudo_password
#             sudo('echo "{key}" >> {file}'.format(
#                 key=key,
#                 file=os.path.join(ssh_dir, 'authorized_keys')))


# def remove_sshkey_from_container(host_obj, container_name, key):
#     """Remove SSH key from the docker container.
#     """
#     from hosts.models import ShipyardInstance
#     shipyard = ShipyardInstance.objects.filter(
#         hosts=host_obj).first()
#     r = requests.get(
#         '{domain}api/v1/containers/?username={user}&api_key={apikey}&limit=0'.format(
#             domain=shipyard.domain,
#             user=shipyard.username,
#             apikey=shipyard.api_key))
#     data = r.json()
#     container_objs = data['objects']
#     for obj in container_objs:
#         if obj['description'] == container_name:
#             volumes = obj['meta']['Volumes']
#             ssh_dir = volumes['/root/.ssh']
#             host_string = "%s@%s" % (
#                 host_obj.main_user, host_obj.ip_address)
#             env.host_string = host_string
#             env.password = host_obj.sudo_password
#             sudo('sed -i \'0,/%s/{//d;}\' %s' % (
#                 key.replace('/', '\/'),
#                 os.path.join(ssh_dir, 'authorized_keys')
#             ))

def sync_domains(webapp_obj):
    from .models import Hipache
    from itertools import groupby
    import re
    domains = webapp_obj.domains.all()
    routers = Hipache.objects.filter(domains__in=domains).distinct()
    for hipache in routers:
        host_obj = hipache.host
        host_string = "%s@%s:%s" % (
            hipache.docker_container.docker_image.main_user,
            host_obj.ip_address,
            hipache.public_ssh_port)
        env.host_string = host_string
        _data = []
        for domain in domains.filter(hipache=hipache).order_by('-is_default'):
            _data.append(
                (
                    domain.domain,
                    '{protocol}://{ip}:{port}'.format(
                        protocol=domain.protocol,
                        ip=domain.webapp.host.ip_address,
                        port=domain.webapp.public_app_port,
                    ),
                )
            )
        _groups = {}
        for key, group in groupby(_data, lambda x: x[0]):
            for w in group:
                if key in _groups:
                        _groups[key].append(w[1])
                else:
                        _groups[key] = [w[1]]

        _pattern = re.compile(r'[\W_]+')
        app_identifier = _pattern.sub('', webapp_obj.name)
        for g in _groups:
            run('redis-cli del frontend:%s' % g)
            run('redis-cli rpush frontend:%s %s' % (
                g,
                app_identifier))
            for i in _groups[g]:
                run('redis-cli rpush frontend:%s %s' % (
                    g,
                    i))


def remove_domains(webapp_obj):
    from .models import Hipache
    domains = webapp_obj.domains.all()
    routers = Hipache.objects.filter(domains__in=domains).distinct()
    for hipache in routers:
        host_obj = hipache.host
        host_string = "%s@%s:%s" % (
            hipache.docker_container.docker_image.main_user,
            host_obj.ip_address,
            hipache.public_ssh_port)
        env.host_string = host_string
        for domain in domains.filter(hipache=hipache):
            run('redis-cli del frontend:%s' % domain.domain)


def update_hipache_db(host_obj):
    host_string = "%s@%s:%s" % (
        host_obj.hipache.docker_container.docker_image.main_user,
        host_obj.ip_address,
        host_obj.hipache.public_ssh_port)
    env.host_string = host_string
    run('redis-cli flushall')

    webapps = host_obj.webapps.all()
    for webapp in webapps:
        sync_domains(webapp)
