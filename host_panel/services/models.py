# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
# from django.conf import settings


class PostgreSQLService(models.Model):
    name = models.CharField(_(u'Name'), max_length=100)
    host = models.ForeignKey(
        'hosts.Host', verbose_name=_(u'Host'),
        related_name='postgresql_servers')
    docker_container = models.OneToOneField(
        'containers.Container', verbose_name=_(u'Container'),
        related_name='postgresql_service',
        null=True, blank=True)
    postgres_password = models.CharField(
        _(u'Password for user "postgres"'), max_length=40)
    public_service_port = models.PositiveIntegerField(
        _(u'Public service port'))
    public_ssh_port = models.PositiveIntegerField(
        _(u'Public SSH port'), null=True, blank=True)
    is_default = models.BooleanField(_(u'Is default'), default=False)

    class Meta:
        verbose_name = _(u'PostgreSQL DB Server')
        verbose_name_plural = _(u'PostgreSQL DB Servers')

    def __unicode__(self):
        return u'PostgreSQL:{0}@{1}'.format(self.name, self.host.name)

    def get_internal_service_port(self):
        return self.docker_container.get_internal_port_by_type('postgres')


class Hipache(models.Model):
    host = models.OneToOneField(
        'hosts.Host', verbose_name=_(u'Host'),
        related_name='hipache')
    docker_container = models.OneToOneField(
        'containers.Container', verbose_name=_(u'Container'),
        related_name='hipache',
        null=True, blank=True)
    public_service_port = models.PositiveIntegerField(
        _(u'Public service port'),
        default=80)
    public_ssh_port = models.PositiveIntegerField(
        _(u'Public SSH port'), null=True, blank=True)

    class Meta:
        verbose_name = _(u'Hipache instance')
        verbose_name_plural = _(u'Hipache instances')

    def __unicode__(self):
        return unicode(self.host)

    def update_hipache_db(self):
        from .utils import update_hipache_db
        update_hipache_db(self.host)
