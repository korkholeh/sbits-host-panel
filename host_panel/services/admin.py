# coding: utf-8
from django.contrib import admin
from .models import (
    PostgreSQLService,
    Hipache,
)


class PostgreSQLServiceAdmin(admin.ModelAdmin):
    list_display = [
        'name', 'host',
        'docker_container',
        'is_default',
        'public_service_port', 'public_ssh_port']
    list_filter = ['host']

admin.site.register(PostgreSQLService, PostgreSQLServiceAdmin)


class HipacheAdmin(admin.ModelAdmin):
    list_display = [
        'host', 'docker_container', 'public_ssh_port']

admin.site.register(Hipache, HipacheAdmin)
