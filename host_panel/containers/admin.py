# coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import (
    DockerImage,
    ExposedPort,
    EnvVariable,
    Container,
    ContainerSSHKey,
)


class ExposedPortInline(admin.TabularInline):
    model = ExposedPort


class EnvVariableInline(admin.TabularInline):
    model = EnvVariable


class DockerImageAdmin(admin.ModelAdmin):
    list_display = [
        'name', 'repository', 'image_name', 'added']
    inlines = [ExposedPortInline, EnvVariableInline]

admin.site.register(DockerImage, DockerImageAdmin)


class ContainerSSHKeyInline(admin.TabularInline):
    model = ContainerSSHKey
    raw_id_fields = ['added_by']


class ContainerAdmin(admin.ModelAdmin):
    list_display = [
        'name', 'host', 'container_id',
        'docker_image', 'is_running',
        'last_sync']
    actions = ['sync_action']
    inlines = [ContainerSSHKeyInline]

    def sync_action(self, request, queryset):
        for q in queryset:
            q.sync()
    sync_action.short_description = _(u'Sync containers')

admin.site.register(Container, ContainerAdmin)
