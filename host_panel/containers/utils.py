# coding: utf-8
from fabric.api import env
from fabric.api import run, sudo
from fabric.contrib import files
import json


def get_container_ids(host_obj):
    host_string = "%s@%s" % (host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    env.password = host_obj.sudo_password
    _running = sudo('docker ps -q --no-trunc')
    _running_ids = _running.splitlines()
    _all = sudo('docker ps -qa --no-trunc')
    _all_ids = _all.splitlines()
    _containers = [(x, x in _running_ids) for x in _all_ids]
    return _containers


def get_container_info(host_obj, container_id):
    host_string = "%s@%s" % (host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    env.password = host_obj.sudo_password
    _r = sudo('docker inspect {0}'.format(container_id))
    return json.loads(_r)


def run_container(name, host_obj, docker_image, env_variables={}):
    host_string = "%s@%s" % (host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    env.password = host_obj.sudo_password
    _cmd = ['sudo docker run -d']
    for p in docker_image.exposed_ports.filter(is_public=True):
        if p.expose_to:
            _cmd.append('-p {0}:{1}'.format(p.expose_to, p.number))
        else:
            _cmd.append('-p {0}'.format(p.number))
    for e in docker_image.env_variables.all():
        _v = env_variables.get(e.name, e.default_value)
        if _v:
            _cmd.append('-e {name}="{value}"'.format(
                name=e.name, value=_v))
    _cmd.append('--name="{0}"'.format(name))
    _cmd.append(docker_image.image_name)
    _cmd.append(docker_image.entrypoint)
    cmd = ' '.join(_cmd)
    _r = sudo(cmd)
    return _r


def destroy_container(container_obj):
    host_obj = container_obj.host
    host_string = "%s@%s" % (host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    env.password = host_obj.sudo_password
    _cmd = ['sudo docker rm -f']
    _cmd.append(container_obj.container_id)
    cmd = ' '.join(_cmd)
    _r = sudo(cmd)
    return _r


def get_container_port(container_obj, port):
    host_obj = container_obj.host
    host_string = "%s@%s" % (host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    env.password = host_obj.sudo_password
    _r = sudo('docker port {0} {1}'.format(
        container_obj.container_id, port))
    if _r.startswith('0.0.0.0'):
        return _r[8:].strip()
    else:
        return None


def save_str_to_file(host_obj, file_name, content):
    host_string = "%s@%s" % (host_obj.main_user, host_obj.ip_address)
    env.host_string = host_string
    env.password = host_obj.sudo_password
    sudo('echo "%s" > %s' % (content.replace('"', '\"'), file_name))
