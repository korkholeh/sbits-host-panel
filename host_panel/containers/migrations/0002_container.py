# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hosts', '__first__'),
        ('containers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Container',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=u'Name')),
                ('host', models.ForeignKey(to='hosts.Host', to_field=u'id', verbose_name=u'Host')),
                ('docker_image', models.ForeignKey(verbose_name=u'Docker image', to_field=u'id', blank=True, to='containers.DockerImage', null=True)),
                ('container_id', models.CharField(max_length=96, null=True, verbose_name=u'Container ID', blank=True)),
                ('meta', models.TextField(default='{}', null=True, verbose_name=u'Meta', blank=True)),
                ('is_running', models.BooleanField(default=True, verbose_name=u'Is running')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name=u'Added')),
                ('last_sync', models.DateTimeField(null=True, verbose_name=u'Last sync', blank=True)),
            ],
            options={
                u'verbose_name': u'Container',
                u'verbose_name_plural': u'Containers',
            },
            bases=(models.Model,),
        ),
    ]
