# encoding: utf8
from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('containers', '0002_container'),
        ('sshkeys', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ContainerSSHKey',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('docker_container', models.ForeignKey(to='containers.Container', to_field=u'id', verbose_name=u'Docker container')),
                ('key', models.ForeignKey(to='sshkeys.SSHKey', to_field=u'id', verbose_name=u'Key')),
                ('added_by', models.ForeignKey(to=settings.AUTH_USER_MODEL, to_field=u'id', verbose_name=u'Added by')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
            ],
            options={
                u'verbose_name': u'PostgreSQL SSH key',
                u'verbose_name_plural': u'PostgreSQL SSH keys',
            },
            bases=(models.Model,),
        ),
    ]
