# encoding: utf8
from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DockerImage',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100, verbose_name=u'Name')),
                ('repository', models.CharField(help_text=u'Public image repository at docker.io.', max_length=100, verbose_name=u'Repository', blank=True)),
                ('image_name', models.CharField(max_length=100, verbose_name=u'Image name')),
                ('description', models.TextField(help_text=u'You can use Markdown here.', verbose_name=u'Description')),
                ('main_user', models.CharField(default='root', max_length=50, verbose_name=u'Main user')),
                ('entrypoint', models.CharField(default='/sbin/my_init --', max_length=300, verbose_name=u'Entrypoint')),
                ('is_ssh', models.BooleanField(default=True, verbose_name=u'Is SSH')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name=u'Added')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name=u'Added')),
                ('is_database_server', models.BooleanField(default=False, verbose_name=u'Is database server')),
                ('is_webapp', models.BooleanField(default=False, verbose_name=u'Is web application')),
            ],
            options={
                u'verbose_name': u'Docker image',
                u'verbose_name_plural': u'Docker images',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EnvVariable',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('docker_image', models.ForeignKey(to='containers.DockerImage', to_field=u'id', verbose_name=u'Docker image')),
                ('name', models.CharField(max_length=100, verbose_name=u'Name')),
                ('default_value', models.CharField(max_length=300, verbose_name=u'Default value', blank=True)),
            ],
            options={
                u'verbose_name': u'Environment variable',
                u'verbose_name_plural': u'Environment variables',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExposedPort',
            fields=[
                (u'id', models.AutoField(verbose_name=u'ID', serialize=False, auto_created=True, primary_key=True)),
                ('docker_image', models.ForeignKey(to='containers.DockerImage', to_field=u'id', verbose_name=u'Docker image')),
                ('number', models.IntegerField(verbose_name=u'Number')),
                ('type', models.CharField(max_length=50, verbose_name=u'Type', choices=[('ssh', u'SSH'), ('webservice', u'Web service'), ('ide', u'IDE'), ('postgres', u'PostgreSQL'), ('redis', u'Redis'), ('memcached', u'Memcached'), ('other', u'Other')])),
                ('is_public', models.BooleanField(default=True, verbose_name=u'Is public')),
                ('expose_to', models.CharField(default='', max_length=100, verbose_name=u'Expose to', blank=True)),
            ],
            options={
                u'verbose_name': u'Exposed port',
                u'verbose_name_plural': u'Exposed ports',
            },
            bases=(models.Model,),
        ),
    ]
