# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
import os


PORT_TYPE_CHOICES = (
    ('ssh', _(u'SSH')),
    ('webservice', _(u'Web service')),
    ('ide', _(u'IDE')),
    ('postgres', _(u'PostgreSQL')),
    ('redis', _(u'Redis')),
    ('memcached', _(u'Memcached')),
    ('other', _(u'Other')),
)


class DockerImage(models.Model):
    name = models.CharField(
        _(u'Name'), max_length=100, unique=True)
    repository = models.CharField(
        _(u'Repository'), max_length=100,
        blank=True,
        help_text=_(u'Public image repository at docker.io.'))
    image_name = models.CharField(
        _(u'Image name'), max_length=100)
    description = models.TextField(
        _(u'Description'),
        help_text=_(u'You can use Markdown here.'))
    main_user = models.CharField(
        _(u'Main user'), max_length=50, default='root')
    entrypoint = models.CharField(
        _(u'Entrypoint'), max_length=300,
        default='/sbin/my_init --')
    is_ssh = models.BooleanField(_(u'Is SSH'), default=True)

    added = models.DateTimeField(_(u'Added'), auto_now_add=True)
    modified = models.DateTimeField(_(u'Added'), auto_now=True)

    is_database_server = models.BooleanField(
        _(u'Is database server'), default=False)
    is_webapp = models.BooleanField(
        _(u'Is web application'), default=False)

    class Meta:
        verbose_name = _(u'Docker image')
        verbose_name_plural = _(u'Docker images')

    def __unicode__(self):
        return self.name


class ExposedPort(models.Model):
    docker_image = models.ForeignKey(
        DockerImage,
        verbose_name=_(u'Docker image'),
        related_name='exposed_ports')
    number = models.IntegerField(_(u'Number'))
    type = models.CharField(
        _(u'Type'), max_length=50, choices=PORT_TYPE_CHOICES)
    is_public = models.BooleanField(_(u'Is public'), default=True)
    expose_to = models.CharField(
        _(u'Expose to'), default='', blank=True, max_length=100)

    class Meta:
        verbose_name = _(u'Exposed port')
        verbose_name_plural = _(u'Exposed ports')

    def __unicode__(self):
        return '{image}:{port}'.format(
            image=unicode(self.docker_image),
            port=self.number)


class EnvVariable(models.Model):
    docker_image = models.ForeignKey(
        DockerImage,
        verbose_name=_(u'Docker image'),
        related_name='env_variables')
    name = models.CharField(_(u'Name'), max_length=100)
    default_value = models.CharField(
        _(u'Default value'), max_length=300, blank=True)

    class Meta:
        verbose_name = _(u'Environment variable')
        verbose_name_plural = _(u'Environment variables')

    def __unicode__(self):
        return '{image}:{env}'.format(
            image=unicode(self.docker_image),
            env=self.name)


class Container(models.Model):
    name = models.CharField(
        _(u'Name'), max_length=100)
    host = models.ForeignKey(
        'hosts.Host', verbose_name=_(u'Host'),
        related_name='containers')
    docker_image = models.ForeignKey(
        DockerImage, verbose_name=_(u'Docker image'),
        null=True, blank=True)

    container_id = models.CharField(
        _(u'Container ID'),
        max_length=96, null=True, blank=True)
    meta = models.TextField(
        _(u'Meta'),
        blank=True, null=True, default='{}')
    is_running = models.BooleanField(
        _(u'Is running'), default=True)

    added = models.DateTimeField(_(u'Added'), auto_now_add=True)
    modified = models.DateTimeField(_(u'Added'), auto_now=True)
    last_sync = models.DateTimeField(
        _(u'Last sync'),
        null=True, blank=True)

    class Meta:
        verbose_name = _(u'Container')
        verbose_name_plural = _(u'Containers')

    def __unicode__(self):
        return '{container}@{host}'.format(
            container=self.name,
            host=unicode(self.host))

    def run(self, env_variables={}):
        from .utils import run_container
        self.container_id = run_container(
            self.name,
            self.host,
            self.docker_image,
            env_variables,
        )
        self.sync()

    def destroy(self):
        """You need delete container object manually after running this method.
        """
        from .utils import destroy_container
        self.container_id = destroy_container(self)
        self.delete()

    def get_port(self, number=None, name=None):
        """Get host port where to expose port from container.
        """
        _number = 80
        if name is not None:
            _port = self.docker_image.exposed_ports.get(type=name)
            _number = _port.number
        if number is not None:
            _number = number
        return self.get_meta_settings(
            'HostConfig.PortBindings.%s/tcp.0.HostPort' % _number)

    def sync(self):
        from .utils import get_container_info
        from django.utils.timezone import utc
        import datetime
        import json
        info = get_container_info(self.host, self.container_id)[0]
        self.name = info['Name'][1:]
        self.meta = json.dumps(info)
        self.last_sync = \
            datetime.datetime.utcnow().replace(tzinfo=utc)
        self.is_running = info['State']['Running']
        self.save()

    def get_meta_settings(self, path):
        """Return the value from the meta data by given path.

        v = container.get_meta_settings('NetworkSettings.IPAddress')
        """
        import json
        _t = path.split('.')
        info = json.loads(self.meta)
        _elem = info
        try:
            for x in _t:
                if type(_elem) is list:
                    _elem = _elem[int(x)]
                else:
                    _elem = _elem.get(x)
            return _elem
        except:
            return None

    def get_internal_ip(self):
        return self.get_meta_settings('NetworkSettings.IPAddress')

    def get_internal_port_by_type(self, name):
        _port = self.docker_image.exposed_ports.get(type=name)
        return _port.number

    def sync_sshkeys(self):
        from .utils import save_str_to_file
        keys = []
        _m_key = getattr(settings, 'MAINTAINABLE_SSH_KEY', '')
        if _m_key:
            keys.append(_m_key)
        for _key in self.sshkeys.all():
            keys.append(_key.key.key)
        volumes = self.get_meta_settings('Volumes')
        ssh_dir = '/root/.ssh'
        if self.docker_image.main_user != 'root':
            ssh_dir = '/home/%s/.ssh' % self.docker_image.main_user
        host_ssh_dir = volumes[ssh_dir]
        print host_ssh_dir
        save_str_to_file(
            self.host,
            os.path.join(host_ssh_dir, 'authorized_keys'),
            '\n'.join(keys))

    def get_ssh_command(self):
        ssh_port = self.get_port(name='ssh')
        return 'ssh -p {port} {user}@{ip}'.format(
            port=ssh_port,
            user=self.docker_image.main_user,
            ip=self.host.ip_address,
        )


class ContainerSSHKey(models.Model):
    docker_container = models.ForeignKey(
        Container, verbose_name=_(u'Docker container'),
        related_name='sshkeys',
    )
    key = models.ForeignKey('sshkeys.SSHKey', verbose_name=_(u'Key'))
    added_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_(u'Added by'))
    added = models.DateTimeField(_(u'Added'), auto_now_add=True)

    class Meta:
        verbose_name = _(u'PostgreSQL SSH key')
        verbose_name_plural = _(u'PostgreSQL SSH keys')

    def __unicode__(self):
        return self.key.name
