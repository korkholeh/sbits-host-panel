��    '      T  5   �      `     a     g  	   p     z  
   �     �     �     �     �     �  
   �     �     �  	             %     3  
   8     C     J  	   ]  
   g     r     �  	   �  	   �     �     �     �     �     �  %   �  
   �       $        <     A     M  �  h     4  
   A     L     _     w  0   �     �     �     �     �               :     X     n     �     �     �     �  ;   �     &	     9	  4   F	     {	  )   �	  %   �	  
   �	  
   �	     �	     �	     
  H   %
     n
  1   �
  7   �
     �
     �
  F   
                          '          	             $          "                                                
                               %      !   &                 #             Added Added by Container Container ID Containers Default value Description Docker container Docker image Docker images Entrypoint Environment variable Environment variables Expose to Exposed port Exposed ports Host Image name Is SSH Is database server Is public Is running Is web application Key Last sync Main user Name Number Other PostgreSQL SSH key PostgreSQL SSH keys Public image repository at docker.io. Repository Sync containers This key already added to container. Type Web service You can use Markdown here. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-06-18 11:22+0300
PO-Revision-Date: 2014-06-18 11:49+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 Додано Додав Контейнер ID контейнеру Контейнери Значення за замовчуванням Опис Контейнер Docker Образ Docker Образи Docker Точка входу Змінна оточення Змінні оточення Розшарити в Розшарений порт Розшарені порти Хост Назва образу Підтримується SSH Підтримується сервер бази даних Публічний Працює Підтримується веб аплікація Ключ Остання синхронізація Головний користувач Назва Число Інші PostgreSQL SSH ключ PostgreSQL SSH ключі Публічний репозиторій образів на docker.io. Репозиторій Синхронізація контейнерів Ключ вже додано до контейнеру. Тип Веб сервіс Можете використовувати розмітку Markdown. 