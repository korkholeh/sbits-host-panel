��    (      \  5   �      p     q     w  	   �     �  
   �     �     �     �     �     �  
   �     �       	        (     5     C  
   H     S     Z  	   m  
   w     �     �  	   �  	   �     �     �     �     �     �     �  %   �  
          $   ,     Q     V     b  �  }     I     Z     i     |     �  (   �     �     �     �          #  '   9  '   a     �     �  !   �     �     �     
	     	     )	     <	     K	     g	  -   p	  '   �	     �	     �	  
   �	     �	     
     
  H   7
     �
  5   �
  9   �
            @   "                          (   !      	             %          #                                               
                               &      "   '                 $              Added Added by Container Container ID Containers Default value Description Docker container Docker image Docker images Entrypoint Environment variable Environment variables Expose to Exposed port Exposed ports Host Image name Is SSH Is database server Is public Is running Is web application Key Last sync Main user Meta Name Number Other PostgreSQL SSH key PostgreSQL SSH keys Public image repository at docker.io. Repository Sync containers This key already added to container. Type Web service You can use Markdown here. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-06-04 12:21+0300
PO-Revision-Date: 2014-06-04 12:31+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 Добавлен Добавил Контейнер ID контейнера Контейнеры Значение по умолчанию Описание Контейнер Docker Образ Docker Образы Docker Точка входа Переменная окружения Переменные окружения Расшарить на Расшаренный порт Расшаренные порты Хост Название образа Есть SSH Сервер БД Публичный Запущен Веб приложение Ключ Последняя синхронизация Главный пользователь Мета данные Название Номер Другое PostgreSQL SSH ключ PostgreSQL SSH ключи Публичный репозиторий образов на docker.io. Репозиторий Синхронизировать контейнеры Ключ уже добавлен к контейнеру. Тип Веб сервис Можете использовать разметку Markdown. 