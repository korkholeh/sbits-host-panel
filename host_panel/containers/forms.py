# coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import ContainerSSHKey


class AddContainerSSHKeyForm(forms.ModelForm):
    class Meta:
        model = ContainerSSHKey
        fields = ['key']

    def __init__(self, customer, container, *args, **kwargs):
        from sshkeys.models import SSHKey
        super(AddContainerSSHKeyForm, self).__init__(*args, **kwargs)
        self.container = container
        self.fields['key'].queryset = SSHKey.objects.filter(owner=customer)

    def clean_key(self):
        already_exist = ContainerSSHKey.objects.filter(
            docker_container=self.container,
            key=self.cleaned_data['key']).exists()

        if not already_exist:
            return self.cleaned_data['key']
        raise forms.ValidationError(_(u'This key already added to container.'))
